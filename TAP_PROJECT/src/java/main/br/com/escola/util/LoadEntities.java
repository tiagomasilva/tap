package br.com.escola.util;

import java.util.ArrayList;
import br.com.escola.dao.DaoFactory;
import br.com.escola.entities.Aluno;
import br.com.escola.entities.Disciplina;
import br.com.escola.entities.Escola;
import br.com.escola.entities.Professor;
import br.com.escola.entities.TipoTurno;
import br.com.escola.entities.Turma;

public class LoadEntities {

	public static void createDummyEntities() {
		createDummyEscola();
		createDummyProfessor();
		createDummyDisciplina();
		createDummyTurma();
		createDummyAluno();
		createDummyEscolaProfessor();
		createDummyDisciplinaProfessor();
		createDummyDisciplinaTurma();
	}

	// Chamadas de Create;
	private static void createDummyEscola() {
		createEscola("GGE");
		createEscola("CAIC");
		createEscola("MARCIEL");
		createEscola("MOZA");
		createEscola("SABINO SABIDO");
	}

	private static void createDummyProfessor() {
		createProfessor("Geise Gi End");
		createProfessor("CARLOS XAVIER");
		createProfessor("Cla AIC");
		createProfessor("MONIC MARCIEL");
		createProfessor("EX MOZA");
	}

	private static void createDummyDisciplina() {
		createDisciplina("Portugues", 85);
		createDisciplina("Matematica", 110);
		createDisciplina("Quimica", 75);
		createDisciplina("Geografia", 75);
		createDisciplina("ED Fisica", 50);
	}

	private static void createDummyTurma() {
		createTurma(TipoTurno.MANHA, 1);
		createTurma(TipoTurno.MANHA, 1);
		createTurma(TipoTurno.NOITE, 2);
		createTurma(TipoTurno.TARDE, 2);
		createTurma(TipoTurno.MANHA, 3);
		createTurma(TipoTurno.MANHA, 3);
		createTurma(TipoTurno.NOITE, 3);
		createTurma(TipoTurno.TARDE, 4);
	}

	private static void createDummyAluno() {
		createAluno("Joao", 1);
		createAluno("Pedritix", 1);
		createAluno("Claudio", 2);
		createAluno("Lenilson", 3);
	}

	private static void createDummyEscolaProfessor() {
		createEscolaProfessor(1, 1);
		createEscolaProfessor(1, 2);
		createEscolaProfessor(2, 1);
		createEscolaProfessor(2, 2);
		createEscolaProfessor(3, 1);
		createEscolaProfessor(3, 2);
		createEscolaProfessor(3, 3);
	}

	private static void createDummyDisciplinaProfessor() {
		createDisciplinaProfessor(1, 1);
		createDisciplinaProfessor(1, 2);
		createDisciplinaProfessor(2, 1);
		createDisciplinaProfessor(2, 2);
		createDisciplinaProfessor(1, 3);
		createDisciplinaProfessor(2, 3);
		createDisciplinaProfessor(3, 3);
	}
	
	private static void createDummyDisciplinaTurma() {
		createDisciplinaTurma(1, 1);
		createDisciplinaTurma(1, 2);
		createDisciplinaTurma(2, 1);
		createDisciplinaTurma(2, 2);
		createDisciplinaTurma(1, 3);
		createDisciplinaTurma(2, 3);
		createDisciplinaTurma(3, 3);
		createDisciplinaTurma(1, 4);
	}

	// Creates

	private static void createEscola(String nomeEscola) {
		Escola escola = new Escola();
		escola.setNomeEscola(nomeEscola);

		DaoFactory.getEscolaDao().insert(escola);
	}

	private static void createProfessor(String nomeProfessor) {
		Professor professor = new Professor();
		professor.setNomeProfessor(nomeProfessor);

		DaoFactory.getProfessorDao().insert(professor);
	}

	private static void createDisciplina(String NomeDisciplina, int cargaHoraria) {
		Disciplina disciplina = new Disciplina();
		disciplina.setNomeDisciplina(NomeDisciplina);
		disciplina.setCargaHoraria(cargaHoraria);

		DaoFactory.getDisciplinaDao().insert(disciplina);
	}

	private static void createTurma(TipoTurno tipoTurno, int idEscola) {
		Escola escola = new Escola();
		escola.setId(idEscola);

		Turma turma = new Turma();
		turma.setTipoTurno(tipoTurno);
		turma.setEscola(escola);

		DaoFactory.getTurmaDao().insert(turma);
	}

	private static void createAluno(String nomeAluno, int idTurma) {
		Turma turma = new Turma();
		turma.setId(idTurma);

		Aluno aluno = new Aluno();
		aluno.setNomeAluno(nomeAluno);
		aluno.setTurma(turma);

		DaoFactory.getAlunoDao().insert(aluno);
	}

	private static void createEscolaProfessor(int idEscola, int idProfessor) {
		Escola escola = DaoFactory.getEscolaDao().find(idEscola);

		Professor professor = DaoFactory.getProfessorDao().find(idProfessor);
		professor.getEscolas().add(escola);

		DaoFactory.getProfessorDao().update(professor);
	}

	private static void createDisciplinaProfessor(int idDisciplina, int idProfessor) {
		Professor professor = DaoFactory.getProfessorDao().find(idProfessor);
		Disciplina disciplina = DaoFactory.getDisciplinaDao().find(idDisciplina);

		disciplina.getProfessores().add(professor);

		DaoFactory.getDisciplinaDao().update(disciplina);
	}
	
	private static void createDisciplinaTurma(int idDisciplina, int idTurma) {
		Disciplina disciplina = DaoFactory.getDisciplinaDao().find(idDisciplina);
		Turma turma = DaoFactory.getTurmaDao().find(idTurma);

		disciplina.getTurmas().add(turma);
		turma.getDisciplinas().add(disciplina);
		
		DaoFactory.getTurmaDao().update(turma);
		DaoFactory.getDisciplinaDao().update(disciplina);
		
	}
}