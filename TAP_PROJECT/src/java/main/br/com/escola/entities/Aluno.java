package br.com.escola.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "aluno")
public class Aluno extends BaseEntity implements Cloneable {
	private String nomeAluno;
	private Turma turma;

	public Aluno() {
	}

	public Aluno(String nomeAluno, Turma turma) {
		setNomeAluno(nomeAluno);
		setTurma(turma);
	}

	@Column(length = 100, nullable = false)
	public String getNomeAluno() {
		return nomeAluno;
	}

	public void setNomeAluno(String nomeAluno) {
		this.nomeAluno = nomeAluno;
	}

	@ManyToOne
	@JoinColumn(name="turma_id", nullable = false, updatable=true)
	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	
	@Override
	public Aluno clone() {
		try {
			return (Aluno) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.getMessage());
		}
	}
}