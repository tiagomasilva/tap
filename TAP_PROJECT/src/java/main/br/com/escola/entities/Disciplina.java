package br.com.escola.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "disciplina")
public class Disciplina extends BaseEntity implements Cloneable {
	private String NomeDisciplina;
	private int cargaHoraria;
	List<Turma> turmas;
	List<Professor> professores;

	public Disciplina() {
	}

	public Disciplina(String NomeDisciplina, int cargaHoraria) {
		setNomeDisciplina(NomeDisciplina);
		setCargaHoraria(cargaHoraria);
	}

	@Column(length = 100, nullable = false)
	public String getNomeDisciplina() {
		return NomeDisciplina;
	}

	public void setNomeDisciplina(String NomeDisciplina) {
		this.NomeDisciplina = NomeDisciplina;
	}

	@Column(nullable = false)
	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	@ManyToMany(mappedBy = "disciplinas")
	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "professor_id", nullable = false, updatable = true)
	public List<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}
}