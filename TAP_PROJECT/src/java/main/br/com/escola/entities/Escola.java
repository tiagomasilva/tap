package br.com.escola.entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "escola")
public class Escola extends BaseEntity implements Cloneable {

	private String nomeEscola;
	private List<Turma> turmas;
	private List<Professor> professores;

	public Escola() {
	}

	public Escola(String nomeEscola) {
		setNomeEscola(nomeEscola);
	}

	@Column(length = 100, nullable = false, unique = true)
	public String getNomeEscola() {
		return nomeEscola;
	}

	public void setNomeEscola(String nomeEscola) {
		this.nomeEscola = nomeEscola;
	}

	@OneToMany(mappedBy = "escola")
	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	@ManyToMany(mappedBy = "escolas")
	public List<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}

	@Override
	public Escola clone() {
		try {
			return (Escola) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.getMessage());
		}
	}
}