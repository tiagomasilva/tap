package br.com.escola.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.jpa.config.Cascade;

@Entity
@Table(name = "turma")
public class Turma extends BaseEntity implements Cloneable {
	private TipoTurno tipoTurno;
	private Escola escola;
	List<Aluno> alunos;
	List<Disciplina> disciplinas;

	public Turma() {
	}

	public Turma(Escola escola, TipoTurno tipoTurno) {
		setEscola(escola);
		setTipoTurno(tipoTurno);
	}
	
	@OneToMany(mappedBy = "turma")
	public List<Aluno> getAlunos() {
		return alunos;
	}
	
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	@ManyToOne
	@JoinColumn(name="escola_id", nullable = false, updatable=true)
	public Escola getEscola() {
		return escola;
	}

	public void setEscola(Escola escola) {
		this.escola = escola;
	}

	@Enumerated(EnumType.ORDINAL)
	public TipoTurno getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(TipoTurno tipoTurno) {
		this.tipoTurno = tipoTurno;
	}
	
//	@JoinColumn(name = "disciplinas_id", nullable = false, updatable = true)
	@ManyToMany(cascade = CascadeType.PERSIST)
	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}
	
	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}
}