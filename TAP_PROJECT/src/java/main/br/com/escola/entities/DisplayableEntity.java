package br.com.escola.entities;

public class DisplayableEntity {

	private int id;
	private String description;

	public int getId() {
		return id;
	}

	public DisplayableEntity(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return id + ": " + description;
	}
	
}
