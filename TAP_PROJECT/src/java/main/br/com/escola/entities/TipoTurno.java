package br.com.escola.entities;

public enum TipoTurno {
	MANHA(0), TARDE(1), NOITE(2);

	private int turno;

	TipoTurno(int turno) {
		this.turno = turno;
	}
}
