package br.com.escola.entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "professor")
public class Professor extends BaseEntity implements Cloneable {
	private String nomeProfessor;
	List<Escola> escolas;
	List<Disciplina> disciplinas;

	public Professor() {
	}

	public Professor(String nomeProfessor) {
		setNomeProfessor(nomeProfessor);
	}

	@Column(length = 100, nullable = false)
	public String getNomeProfessor() {
		return nomeProfessor;
	}

	public void setNomeProfessor(String nomeProfessor) {
		this.nomeProfessor = nomeProfessor;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "escola_id", nullable = false, updatable = true)
	public List<Escola> getEscolas() {
		return escolas;
	}

	public void setEscolas(List<Escola> escolas) {
		this.escolas = escolas;
	}

	@ManyToMany(mappedBy = "professores")
	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

}