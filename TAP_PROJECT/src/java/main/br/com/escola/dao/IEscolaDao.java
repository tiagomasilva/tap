package br.com.escola.dao;

import br.com.escola.entities.Escola;

public interface IEscolaDao extends IDao<Escola> {

}