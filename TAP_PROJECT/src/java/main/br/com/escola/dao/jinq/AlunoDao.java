package br.com.escola.dao.jinq;

import java.util.Map;
import java.util.stream.Collectors;
import org.jinq.orm.stream.JinqStream;
import org.jinq.tuples.Pair;
import br.com.escola.dao.IAlunoDao;
import br.com.escola.entities.TipoTurno;
import br.com.escola.entities.Aluno;

public class AlunoDao extends GenericDaoJinq<Aluno> implements IAlunoDao {

	public AlunoDao() {
		super(Aluno.class);
	}

	public  Map<String, Long> getAlunosEDisciplina(TipoTurno turno) {
		try{
		return getStream().where(aluno -> aluno.getTurma().getTipoTurno().equals(turno))
				.select( aluno -> new Pair<>(aluno,aluno.getTurma()) )
				.join(a -> JinqStream.from(a.getTwo().getDisciplinas()))
				.group(x -> x.getOne().getOne().getNomeAluno(), (Disciplina,dis) -> dis.select(d -> d.getTwo()).count())
				.collect(Collectors.toMap(Pair::getOne,Pair::getTwo));
		}catch (Exception e) {
			return null;
		}
	}
}