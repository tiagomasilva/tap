package br.com.escola.dao.jinq;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Tiago e Filipe
 *
 */
public class EntityManagerFactoryLocator {
	
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("TAP_PROJECT");
	
	public static EntityManagerFactory getFactory() {
		return factory;
	}
}
