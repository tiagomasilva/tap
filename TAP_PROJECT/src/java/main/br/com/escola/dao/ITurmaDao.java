package br.com.escola.dao;

import br.com.escola.entities.Turma;

public interface ITurmaDao extends IDao<Turma> {

}