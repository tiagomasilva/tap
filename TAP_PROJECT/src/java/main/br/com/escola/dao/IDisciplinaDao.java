package br.com.escola.dao;

import br.com.escola.entities.Disciplina;

public interface IDisciplinaDao extends IDao<Disciplina> {

}