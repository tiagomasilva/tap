package br.com.escola.dao;

import br.com.escola.entities.Professor;

public interface IProfessorDao extends IDao<Professor> {

}