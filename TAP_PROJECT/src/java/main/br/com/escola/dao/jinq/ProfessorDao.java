package br.com.escola.dao.jinq;

import org.jinq.orm.stream.JinqStream;
import br.com.escola.dao.IProfessorDao;
import br.com.escola.entities.Professor;

public class ProfessorDao extends GenericDaoJinq<Professor> implements IProfessorDao {

	public ProfessorDao() {
		super(Professor.class);
	}

	public boolean isProfessorMaisDisciplinas(String nomeProfessor) {
		try{
			return getStream()
					.join(professor -> JinqStream.from(professor.getDisciplinas()))
					.group(p -> p.getOne().getNomeProfessor(), (Disciplina,dis) -> dis.count())
					.sortedDescendingBy(qtd -> qtd.getTwo())
					.findFirst()
					.get()
					.getOne().equalsIgnoreCase(nomeProfessor);
		}catch (Exception e) {
			return false;
		}
	}
}