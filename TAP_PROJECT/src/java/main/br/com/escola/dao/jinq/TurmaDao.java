package br.com.escola.dao.jinq;

import org.jinq.orm.stream.JinqStream;
import br.com.escola.dao.ITurmaDao;
import br.com.escola.entities.Turma;

public class TurmaDao extends GenericDaoJinq<Turma> implements ITurmaDao {

	public TurmaDao() {
		super(Turma.class);
	}

	public long getCargaHorariaTotal(String nomeEscola) {
		try {
			return getStream()
					.where(turma -> turma.getEscola().getNomeEscola().toUpperCase().equals(nomeEscola.toUpperCase()))
					.join(turma -> JinqStream.from(turma.getDisciplinas()))
					.sumInteger(par -> par.getTwo().getCargaHoraria());
		} catch (Exception e) {
			return 0;

		}
	}
}