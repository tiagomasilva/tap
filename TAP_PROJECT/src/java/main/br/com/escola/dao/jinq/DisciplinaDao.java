package br.com.escola.dao.jinq;

import java.util.List;
import java.util.stream.Collectors;
import br.com.escola.dao.IDisciplinaDao;
import br.com.escola.entities.Disciplina;

public class DisciplinaDao extends GenericDaoJinq<Disciplina> implements IDisciplinaDao {

	public DisciplinaDao() {
		super(Disciplina.class);
	}

	public List<String> getMaioresCargaHoraria(int quantity) {
		try{
			
		return getStream()
				.sortedDescendingBy(dis -> dis.getCargaHoraria())
				.limit(quantity)
				.map(dis -> dis.getNomeDisciplina())
				.collect(Collectors.toList());
		}catch (Exception e) {
			return null;
		}
	}
}