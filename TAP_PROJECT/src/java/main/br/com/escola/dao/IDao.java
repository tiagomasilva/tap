package br.com.escola.dao;

import java.util.List;

import br.com.escola.dao.jinq.IWhere;
import br.com.escola.entities.BaseEntity;

public interface IDao<T extends BaseEntity> {

	void insert(T t);
	void update(T t);
	void delete(int id);
	T find(int id);
	List<T> retrieve();
	List<T> retrieve(IWhere<T> predicate);
}
