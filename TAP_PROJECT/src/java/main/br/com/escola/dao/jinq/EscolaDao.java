package br.com.escola.dao.jinq;

import br.com.escola.dao.IEscolaDao;
import br.com.escola.entities.Escola;

public class EscolaDao extends GenericDaoJinq<Escola> implements IEscolaDao {

	public EscolaDao() {
		super(Escola.class);
	}

	public Escola getEscolaMaisProfessores() {
		try{
		return getStream().group(escola -> escola, (Professor, prof) -> prof.count())
				.sortedDescendingBy(s -> s.getTwo())
				.findFirst().get().getOne();
		}catch (Exception e) {
			return null;
		}
	}
}