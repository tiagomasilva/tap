package br.com.escola.dao;

import br.com.escola.dao.jinq.GenericDaoJinq;
import br.com.escola.entities.Aluno;
import br.com.escola.entities.Disciplina;
import br.com.escola.entities.Escola;
import br.com.escola.entities.Professor;
import br.com.escola.entities.Turma;

public class DaoFactory {

	public static IDao<Escola> getEscolaDao() {
		return new GenericDaoJinq<>(Escola.class);
	}

	public static IDao<Professor> getProfessorDao() {
		return new GenericDaoJinq<>(Professor.class);
	}

	public static IDao<Disciplina> getDisciplinaDao() {
		return new GenericDaoJinq<>(Disciplina.class);
	}

	public static IDao<Turma> getTurmaDao() {
		return new GenericDaoJinq<>(Turma.class);
	}

	public static IDao<Aluno> getAlunoDao() {
		return new GenericDaoJinq<>(Aluno.class);
	}
}
