package br.com.escola.main;

import br.com.escola.dao.jinq.AlunoDao;
import br.com.escola.dao.jinq.DisciplinaDao;
import br.com.escola.dao.jinq.EscolaDao;
import br.com.escola.dao.jinq.ProfessorDao;
import br.com.escola.dao.jinq.TurmaDao;
import br.com.escola.entities.Escola;
import br.com.escola.entities.TipoTurno;
import br.com.escola.util.LoadEntities;

public class Main {

	public static void main(String[] args) {
		LoadEntities.createDummyEntities();

		TurmaDao turma = new TurmaDao();
		System.err.println("1 ----- Carga Horaria Total da Disciplica da Escola -----");
		System.err.println(turma.getCargaHorariaTotal("GGE"));
		System.err.println(turma.getCargaHorariaTotal("CAIC"));

		
		System.err.println("2 ----- � o Professor com Mais Disciplina? -----");
		ProfessorDao professor = new ProfessorDao();
		System.err.println(professor.isProfessorMaisDisciplinas("EX MOZA"));
		System.err.println(professor.isProfessorMaisDisciplinas("Cla AIC"));
		
		System.err.println("3 ----- nome dos alunos e a quantidade de disciplinas que os mesmos est�o matriculados. -----");
		AlunoDao aluno = new AlunoDao();
		aluno.getAlunosEDisciplina(TipoTurno.MANHA)
		.forEach((a,q) -> System.out.println(a + " " + q));
		aluno.getAlunosEDisciplina(TipoTurno.NOITE)
		.forEach((a,q) -> System.out.println(a + " " + q));
		
		System.err.println("4 ----- Escola com mais Professor -----");
		EscolaDao escolaMais = new EscolaDao();
		Escola esc = escolaMais.getEscolaMaisProfessores();
		System.err.println((esc!=null)?esc.getNomeEscola():" ");

		
		System.err.println("5 ----- disciplinas que possuem maior carga hor�ria -----");
		DisciplinaDao dis = new DisciplinaDao();
		int quantity = 3;
		for (String nomeDisciplinas : dis.getMaioresCargaHoraria(quantity))
			System.err.println(nomeDisciplinas);
	}

}
