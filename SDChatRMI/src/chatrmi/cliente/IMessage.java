package chatrmi.cliente;

import java.rmi.Remote;
import java.rmi.RemoteException;

import chatrmi.model.TypeMessage;

public interface IMessage extends Remote {

	public String getMessage() throws RemoteException;
	public IUser getUserEmissor() throws RemoteException;
	public IUser getUserDestinatario() throws RemoteException;
	public TypeMessage getTypeMessage() throws RemoteException;

}