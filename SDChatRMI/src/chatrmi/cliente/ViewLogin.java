package chatrmi.cliente;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import chatrmi.controller.Controller;

public class ViewLogin extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JTextField txtLogin, txtIp;
	public JLabel lblMessage;
	private JButton btnEntrar;

	public ViewLogin() {
		
//		try {
//			String tema = "bernstein.Bernstein";
//			UIManager.setLookAndFeel("com.jtattoo.plaf."+tema+"LookAndFeel");
//		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
//			e1.printStackTrace();
//		}
//		Properties props = new Properties();
//		props.put("logoString", "");

		add(new JLabel("Informe seus dados:")).setBounds(10, 25, 254, 14);
		add(new JLabel("Login:")).setBounds(10, 55, 55, 20);
		add(new JLabel("IP:")).setBounds(10, 80, 55, 17);
		
		
		txtLogin = new JTextField();
		txtLogin.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					acessar();
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
			}
		});
		add(txtLogin).setBounds(55, 55, 195, 20);

		txtIp = new JTextField();
		txtIp.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					acessar();
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
			}
		});
		add(txtIp).setBounds(55, 80, 195, 20);

		btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(this);
		add(btnEntrar).setBounds(150, 110, 100, 25);

		lblMessage = new JLabel("");
		add(lblMessage).setBounds(10, 132, 195, 20);

		setUndecorated(true);
		setLayout(null);
		setVisible(true);
		setSize(260, 160);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void acessar(){
		String ip = txtIp.getText().trim();
		ip = ip.equals("") ? "localhost" : ip;
		try {
			Controller.conectar(ip, txtLogin.getText());
			new ViewChat(txtLogin.getText().trim());
			dispose();
		} catch (RemoteException | NotBoundException ex) {
			connectionError(ip, txtLogin.getText().trim());
		}
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!txtLogin.getText().trim().equals("")) {
			acessar();
		} else
			JOptionPane.showMessageDialog(null, "LOGIN INV�LIDO", "Aten��o", JOptionPane.ERROR_MESSAGE);
	}

	private void connectionError(String ip, String login) {
		new Thread() {
			public void run() {
				btnEntrar.setEnabled(false);
				for (int i = 0; i < 5; i++) {
					try {
						lblMessage.setText("Conectando...");
						System.out.println("Tentativa: "+(i+1));
						Controller.conectar(ip, login);
						new ViewChat(login.trim());
						dispose();
						break;
					} catch (RemoteException | NotBoundException e) {
						try {
							for (int t = 6; t >= 0; t--) {
								if (t == 6 || t==0) 
									lblMessage.setText("Servi�o est� indispon�vel");
								else{ 
									lblMessage.setText("Tentando em " +t+ " segundo(s).");
									sleep(TimeUnit.SECONDS.toMillis(1));
								}
							}
						} catch (InterruptedException ex) {
							ex.printStackTrace();
						}
					}
				}
				btnEntrar.setEnabled(true);
			}
		}.start();
	}

}