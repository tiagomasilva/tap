package chatrmi.cliente;

import javax.swing.JTextArea;

public class JTextAba {
	
	private String login;
	private JTextArea txtArea;
	
	public JTextAba(String login, JTextArea txtArea) {
		setLogin(login);
		setTxtArea(txtArea);
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public JTextArea getTxtArea() {
		return txtArea;
	}
	
	public void setTxtArea(JTextArea txtArea) {
		this.txtArea = txtArea;
	}
	
}