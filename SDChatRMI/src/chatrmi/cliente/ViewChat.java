package chatrmi.cliente;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.ConnectException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import chatrmi.controller.Controller;
import chatrmi.model.UserImpl;

public class ViewChat extends JFrame {

	private static final long serialVersionUID = -7539672037068469518L;

	private JPanel contentPane;
	private JTabbedPane jTab;
	private JTextArea txtHistorico, txtMsg;
	private JButton btnEnviar, btnSair;
	private JList<String> lista;
	private DefaultListModel<String> model;
	private ArrayList<JTextAba> listTab;

	public ViewChat(final String login) {
		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		add(new JLabel("Logado como: " + login)).setBounds(10, 5, 500, 20);

		jTab = new JTabbedPane();
		add(jTab).setBounds(10, 30, 350, 300);

		txtHistorico = new JTextArea();
		txtHistorico.setLineWrap(true);
		txtHistorico.setEditable(false);
		txtHistorico.setBounds(10, 30, 350, 300);
		listTab = new ArrayList<JTextAba>();
		listTab.add(new JTextAba("geral", txtHistorico));

		jTab.addTab("geral", null, new JScrollPane(listTab.get(0).getTxtArea(),
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), null);
		jTab.setForegroundAt(0, Color.red);
		jTab.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && jTab.getSelectedIndex() != 0) {
					jTab.remove(jTab.getSelectedComponent());
				}
			}
		});

		add(new JLabel("Conectados:")).setBounds(375, 5, 100, 20);

		model = new DefaultListModel<String>();
		lista = new JList<String>(model);
		lista.scrollRectToVisible(new Rectangle());
		lista.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2)
					if (!abaAberta(lista.getSelectedValue())
							&& !lista.getSelectedValue()
							.equalsIgnoreCase(login))
						addAba(lista.getSelectedValue());
			}
		});
		add(
				new JScrollPane(lista, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_NEVER)).setBounds(375,
								30, 115, 300);

		txtMsg = new JTextArea();
		txtMsg.setLineWrap(true);

		txtMsg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					enviarMensagem(txtMsg.getText().trim());
					txtMsg.setText("");
					txtMsg.requestFocus();
					System.out.println(e.getKeyCode());
				}
			}
		});
		txtMsg.setBounds(10, 340, 410, 70);
		add(
				new JScrollPane(txtMsg, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_NEVER)).setBounds(10,
								340, 410, 70);

		btnEnviar = new JButton("OK");

		btnEnviar.addActionListener(i -> {
			enviarMensagem(txtMsg.getText().trim());
			txtMsg.setText("");
			txtMsg.requestFocus();
		});
		add(btnEnviar).setBounds(430, 340, 60, 40);

		btnSair = new JButton("Sair");
		btnSair.addActionListener(i -> sairSistema());
		add(btnSair).setBounds(430, 390, 60, 20);

		new ThreadMensagens();
		new ThreadConectados();

		setVisible(true);
		setSize(510, 455);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				sairSistema();
			}
		});
		txtMsg.requestFocus();
	}

	public boolean inList(String usuario) {
		for (JTextAba lista : listTab) {
			if (lista.getLogin().equalsIgnoreCase(usuario))
				return true;
		}
		return false;
	}

	public void addAba(String usuario) {
		JTextArea txt = new JTextArea();
		txt.setLineWrap(true);
		txt.setEditable(false);
		txt.setBounds(10, 30, 350, 300);

		if (!inList(usuario)) {
			listTab.add(new JTextAba(usuario, txt));
		}

		listTab.forEach(txtArea -> {
			if (txtArea.getLogin().equalsIgnoreCase(usuario))
				jTab.addTab(usuario, null, new JScrollPane(
						txtArea.getTxtArea(),
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), null);
		});

	}

	private void sairSistema() {
		Controller.desconectar();
		System.exit(0);
	}

	private boolean abaAberta(String usuario) {
		boolean b = false;
		for (int i = 0; i < jTab.getTabCount(); i++)
			if (jTab.getTitleAt(i).equalsIgnoreCase(usuario))
				b = true;
		return b;
	}

	private void enviarMensagem(String mensagem) {
		for (int i = 0; i < jTab.getTabCount(); i++)
			if (jTab.getComponentAt(i).isVisible())
				if (jTab.getTitleAt(i).equalsIgnoreCase("geral"))
					Controller.enviarMensagem(txtMsg.getText(), null);
				else {
					if(Controller.isServerStatus() == true) {
						try {
							if (Controller.usuarioConectado(jTab.getTitleAt(i)))
								Controller.enviarMensagem(txtMsg.getText(),
										jTab.getTitleAt(i));
							else
								throw new UsuarioDesconectado(jTab.getTitleAt(i));

						} catch (UsuarioDesconectado e) {
						}
					}
					else{
								Controller.enviarMensagem(txtMsg.getText(),
										jTab.getTitleAt(i));
						
					}

				}
	}

	private class ThreadMensagens extends Thread {

		public ThreadMensagens() {
			start();
		}

		@Override
		public void run() {
			super.run();
				do {
					if (Controller.atualizarMensagensPublicas()) {
						listTab.forEach(tab -> {
							if (tab.getLogin().equalsIgnoreCase("geral"))
								tab.getTxtArea().setText(tab.getTxtArea().getText()+ Controller.getMensagensPublicas());
						});
					}

					try{
						try{
							Controller.getUsers().forEach(user -> {
								try {
									if (!user.getLogin().equalsIgnoreCase(Controller.usuario.getLogin())) {
										try {
											if (Controller.atualizarMensagensPrivadas(user.getLogin())) {
												if (!abaAberta(user.getLogin()))addAba(user.getLogin());
												listTab.forEach(tab -> {
													try {
														if (tab.getLogin().equalsIgnoreCase(user.getLogin()))
															tab.getTxtArea().setText(tab.getTxtArea().getText()+ Controller.getMensagensPrivadas(user.getLogin()));
													} catch (Exception e) {
														e.printStackTrace();
													}
												});
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							});
							sleep(1000);
						} catch (NullPointerException | ConnectException e1){
							Controller.getUsers().addElement(new UserImpl("NENHUM CONECTADOS"));
						}
					} catch (Exception e) {
						Controller.desregistrarServidor();
						Controller.desregistrarCliente();
						Controller.tentarConectar();
					}
			} while (Controller.isServerStatus());
		}

	}

	private class ThreadConectados extends Thread {

		public ThreadConectados() {
			start();
		}

		@Override
		public void run() {
			while (true){
				try {
					sleep(1000);
					if (Controller.atualizarConectados()) {
						model.clear();
						try{
							for (IUser element : Controller.getUsers())
								model.addElement(element.getLogin());
						} catch (NullPointerException | ConnectException e1){
							Controller.getUsers().add(new UserImpl("NENHUM CONECTADO"));
						}
					}
				} catch (Exception e) {
					Controller.desregistrarServidor();
					Controller.desregistrarCliente();
					Controller.tentarConectar();
				}
			}

		}

	}

}