package chatrmi.cliente;

import javax.swing.JOptionPane;

public class UsuarioDesconectado extends Exception {
	
	private static final long serialVersionUID = 1L;

	public UsuarioDesconectado(String usuario) {
		JOptionPane.showMessageDialog(null, "N�o foi poss�vel enviar mensagem. \nUsu�rio "+usuario+" desconectou-se!", "Aten��o", JOptionPane.WARNING_MESSAGE);
	}

}