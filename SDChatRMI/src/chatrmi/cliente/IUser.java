package chatrmi.cliente;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IUser extends Remote {

	public String getLogin() throws RemoteException;
	
}