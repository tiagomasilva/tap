package chatrmi.controller;

import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

import javax.swing.JOptionPane;

import chatrmi.cliente.IUser;
import chatrmi.model.UserImpl;
import chatrmi.server.IServicoChat;

public class Controller {
	
	private static final int PORT_IP_SERV = 4000;
	private static final String SERVICO_CHAT = "ServicoChatOnline";
	
	private static IServicoChat servChat;
	public static UserImpl usuario;
	
	private static int contadorUsuarios;
	private static boolean serverStatus;
	
	private static Registry registryServer, registryCliente;
	
	private static String ipCliente, loginCliente;

	public static void setServerStatus(boolean serverStatus) {
		Controller.serverStatus = serverStatus;
	}
	public static boolean isServerStatus() {
		return serverStatus;
	}

	public static void tentarConectar(){
		try {
			conectar(ipCliente, loginCliente);
		} catch (Exception e) {
			System.out.println("Tentando conectar ...");
			tentarConectar();
		}
	}
	
	public static void conectar(String ip, String login) throws RemoteException, NotBoundException, ConnectException {
		registrarServidor(ip);
		registrarCliente(login);
		
		ipCliente = ip;
		loginCliente = login;
		
		usuario = new UserImpl(login);
		servChat.conectar(usuario);
		
		contadorUsuarios = 0;
		setServerStatus(true);
		System.out.println("Usuário conectado como: "+usuario.getLogin());
	}
	
	public static void registrarServidor(String ip) throws RemoteException, NotBoundException {
		registryServer = LocateRegistry.getRegistry(ip, PORT_IP_SERV);
		servChat = (IServicoChat) registryServer.lookup(SERVICO_CHAT);
	}
	
	public static void desregistrarServidor(){
	    if (registryServer != null){
	        try{
	            UnicastRemoteObject.unexportObject(registryServer, true);
	        }catch (NoSuchObjectException e){}
	        registryServer = null;
	    }
	}
	
	public static void registrarCliente(String login) {
		try {
			if (System.getSecurityManager() == null)
				System.setSecurityManager(new SecurityManager());
			registryCliente = LocateRegistry.createRegistry(servChat.incPort());
			System.out.println("Servico de CLIENTE registrado com sucesso");
		} catch (Exception e) {
			System.out.println("Erro ao registrar Servico de CLIENTE");
			e.printStackTrace();
		}
	}
	
	public static void desregistrarCliente(){
	    if (registryCliente != null){
	        try{
	            UnicastRemoteObject.unexportObject(registryCliente, true);
	        }catch (NoSuchObjectException e){}
	        registryCliente = null;
	    }
	}

	public static void desconectar(){
		try {
			servChat.desconectar(usuario);
			System.out.println("Usuário: "+usuario.getLogin()+" DESCONECTADO!");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public static void enviarMensagem(final String mensagem, final String destinatario){
		try{
			if(!mensagem.trim().equals(""))
				if(servChat.getConectados() == 1)
					JOptionPane.showMessageDialog(null, "Apenas você "+usuario.getLogin()+", está conectado!", "Atenção", JOptionPane.WARNING_MESSAGE);
				else
					if(destinatario == null)
						servChat.enviarPublico(usuario, mensagem);
					else
						servChat.enviarPrivado(usuario, mensagem, new UserImpl(destinatario));
			System.out.println("Há conectados: "+servChat.getConectados());
		} catch (RemoteException e) {
			try{
				setServerStatus(false);
				if(destinatario == null){
					System.out.println("PÚBLICO");
					tentarEnviarMensagemPublica(usuario, mensagem);
				}else{
					System.out.println("PRIVADO");
					tentarEnviarMensagemPrivada(usuario, mensagem, new UserImpl(destinatario));
				}
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static void tentarEnviarMensagemPublica(final IUser u, final String mensagem){
		new Thread(){
			public void run(){
				try {
					enviar();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			private void enviar() throws InterruptedException {
				try{
					sleep(2000);
					servChat.enviarPublico(u, mensagem);
					System.out.println("Mensagem enviada");
					setServerStatus(true);
				} catch (RemoteException e) {
//					e.printStackTrace();
					try {
						System.out.println(u.getLogin()+": tentando enviar..." + mensagem);
					} catch (RemoteException e1) {}
					enviar();
				}
			}
		}.start();
	}
	private static void tentarEnviarMensagemPrivada(final IUser u,final String mensagem, final IUser d){
		new Thread(){
			public void run(){
				try {
					enviar();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			private void enviar() throws InterruptedException{
				try {
					sleep(2000);
					servChat.enviarPrivado(u, mensagem, d);
					System.out.println("Mensagem enviada");
					setServerStatus(true);
				} catch (RemoteException e) {
//					e.printStackTrace();
					try {
						System.out.println(u.getLogin()+": tentando enviar..." + mensagem+ " para: "+d.getLogin());
					} catch (RemoteException e1) {}
					enviar();
				}
			}
		}.start();
	} 

	public static boolean usuarioConectado(String login){
		try {
			return servChat.getLoginConectados().contains(login);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String getMensagensPrivadas(String privado){
		String message;
		try {
			message = servChat.mensagemPrivada(usuario.getLogin(), privado).toString();
			servChat.lerMensagemPrivada(usuario.getLogin(), privado);
			return message;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getMensagensPublicas(){
		try {
			/*Vector<MessageImpl> m = servChat.getMessagesPublicasNaoLidas(usuario.getLogin());
			String mensagens = "";
			for (MessageImpl ms: m)
				mensagens += ms.getUserEmissor().getLogin()+": "+ms.getMessage()+"\n";*/
			String message = servChat.mensagemPublica(usuario.getLogin()).toString();
			servChat.lerMensagem(usuario.getLogin());
			return message;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Vector<IUser> getUsers() throws NullPointerException, ConnectException {
		try {
			return servChat.getUsersConectados();
		} catch (RemoteException e) {
			//e.printStackTrace();
		}
		return null;
	}
	
	public static boolean atualizarMensagensPublicas(){
		try {
			return servChat.atualizarMessagesPublicasNaoLidas(usuario.getLogin());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean atualizarMensagensPrivadas(String privado){
		try {
			return servChat.atualizarMessagesPrivadasNaoLidas(usuario.getLogin(), privado);
		} catch (RemoteException e) {
			//e.printStackTrace();
		}
		return false;
	}

	public static boolean atualizarConectados() throws ConnectException {
		try {
			if(isServerStatus()){
				if(contadorUsuarios < servChat.getUsersConectados().size()){
					contadorUsuarios +=1;
					return true;
				}else if(contadorUsuarios > servChat.getUsersConectados().size()){
					contadorUsuarios -=1;
					return true;
				}
			}
		} catch (Exception e) {
			setServerStatus(false);
//			e.printStackTrace();
		}
		return false;
	}

}