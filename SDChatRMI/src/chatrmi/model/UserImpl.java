package chatrmi.model;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import chatrmi.cliente.IUser;

public class UserImpl extends UnicastRemoteObject implements IUser {

	private static final long serialVersionUID = 6087440074417431467L;
	
	private String login;

	public UserImpl(String login) throws RemoteException {
		setLogin(login);
	}
	
	@Override
	public  String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public boolean equals(Object obj) {
		return getLogin().equals(((UserImpl)obj).getLogin());
	}

}