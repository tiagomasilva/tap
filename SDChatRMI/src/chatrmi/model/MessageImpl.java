package chatrmi.model;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import chatrmi.cliente.IMessage;
import chatrmi.cliente.IUser;

public class MessageImpl extends UnicastRemoteObject implements IMessage {
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	private IUser userEmissor, userDestinatario;
	private TypeMessage type;
	
	public MessageImpl(String message, IUser userEmissor, TypeMessage type) throws RemoteException {
		setMessage(message);
		setUserEmissor(userEmissor);
		setTypeMessage(type);
	}
	
	public MessageImpl(IUser userEmissor, String message, IUser userDestinatario, TypeMessage type) throws RemoteException {
		setUserEmissor(userEmissor);
		setMessage(message);
		setUserDestinatario(userDestinatario);
		setTypeMessage(type);
	}
	
	@Override
	public String getMessage() throws RemoteException {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public IUser getUserEmissor() throws RemoteException {
		return userEmissor;
	}

	public void setUserEmissor(IUser userEmissor) {
		this.userEmissor = userEmissor;
	}

	@Override
	public IUser getUserDestinatario() throws RemoteException {
		return userDestinatario;
	}

	public void setUserDestinatario(IUser userDestinatario) {
		this.userDestinatario = userDestinatario;
	}

	@Override
	public TypeMessage getTypeMessage() throws RemoteException {
		return this.type;
	}
	
	public void setTypeMessage(TypeMessage type){
		this.type = type;
	}

}