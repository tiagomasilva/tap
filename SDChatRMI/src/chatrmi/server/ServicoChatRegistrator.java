package chatrmi.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServicoChatRegistrator {
	
	private static final String SERVICO_CHAT = "ServicoChatOnline";
	private static final int PORT_IP = 4000;
	
	public static void main(String[] args) {
		try {
			if (System.getSecurityManager() == null)
				System.setSecurityManager(new SecurityManager());
			
			IServicoChat servidorChat = new ServicoChatImpl();
			IServicoChat stub = (IServicoChat) UnicastRemoteObject.exportObject(servidorChat, PORT_IP);
			
			Registry registry = LocateRegistry.createRegistry(PORT_IP);
			registry.rebind(SERVICO_CHAT, stub);
			
			System.out.println("Servico de CHAT registrado com sucesso");
		} catch (Exception e) {
			System.out.println("Erro ao registrar Servico de CHAT");
			e.printStackTrace();
		}
	} 

}