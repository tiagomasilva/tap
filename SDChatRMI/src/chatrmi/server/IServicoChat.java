package chatrmi.server;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import chatrmi.cliente.IUser;
import chatrmi.model.MessageImpl;

public interface IServicoChat extends Remote {
	
     public void conectar(IUser u) throws RemoteException;
     public void enviarPublico(IUser u, String mensagem) throws RemoteException;
     public void enviarPrivado(IUser e, String mensagem, IUser d) throws RemoteException;
     public void desconectar(IUser u) throws RemoteException;
     public Vector<MessageImpl> getMessagesPublicas(String login) throws RemoteException;
     public Vector<MessageImpl> getMessagesPrivadas(String emissor, String destinatario) throws RemoteException;
     public boolean atualizarMessagesPublicasNaoLidas(String login) throws RemoteException;
     public boolean atualizarMessagesPrivadasNaoLidas(String login, String privado) throws RemoteException;
     public Vector<MessageImpl> getMessagesPrivadasNaoLidas(String emissor, String destinatario) throws RemoteException;
     public int getConectados() throws RemoteException;
     public List<String> getLoginConectados() throws RemoteException;
     public Vector<IUser> getUsersConectados() throws RemoteException;
     public int incPort() throws RemoteException;
     public void lerMensagem(String login) throws RemoteException;
     public void lerMensagemPrivada(String login, String privado) throws RemoteException;
     public StringBuilder mensagemPublica(String login) throws RemoteException;
     public StringBuilder mensagemPrivada(String login, String privado) throws RemoteException;
}