package chatrmi.server;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import chatrmi.cliente.IUser;
import chatrmi.model.MessageImpl;
import chatrmi.model.TypeMessage;

public class Conversa {
	
	private final String GROUP = "geral";
	private static HashMap<String, HashMap<String, MessageListImpl>> map;
	
	public Conversa() {
		map = new HashMap<String, HashMap<String, MessageListImpl>>();
	}
	
	public void registrarUsuario(String login){
		map.put(login, new HashMap<String, MessageListImpl>());
		map.get(login).put(GROUP, new MessageListImpl());
	}
	
	private void adicionarPrivado(String login, String privado){
		if(!existePrivado(login, privado)){
			map.get(login).put(privado, new MessageListImpl());
			map.get(privado).put(login, new MessageListImpl());
		}
	}

	public boolean existePrivado(String login, String privado){
		return map.get(login).containsKey(privado);
	}

	public Vector<MessageImpl> buscarConversaPublica(String login) throws RemoteException {
		return map.get(login).get(GROUP).getHistorico();
	}
	
	public Vector<MessageImpl> buscarConversaPrivada(String login, String privado) throws RemoteException {
		return map.get(login).get(privado).getHistorico();
	}
	
	public Vector<MessageImpl> buscarConversaPublicaNaoLida(String login) throws RemoteException {
		return map.get(login).get(GROUP).getMessageNaoLidas();
	}

	public Vector<MessageImpl> buscarConversaPrivadaNaoLida(String login, String privado) throws RemoteException {
		return map.get(login).get(privado).getMessageNaoLidas();
	}
	
	public void lerMessage(String login) throws RemoteException {
		map.get(login).get(GROUP).changeType();
	}
	
	public void lerMessagePrivada(String login, String privado) throws RemoteException {
		map.get(login).get(privado).changeType();
	}
	
	public void adicionarConversaPrivada(IUser emissor, String mensagem, IUser destinatario) {
		try {
			adicionarPrivado(emissor.getLogin(), destinatario.getLogin());
			map.get(emissor.getLogin()).get(destinatario.getLogin()).addMessage(new MessageImpl(emissor, mensagem, destinatario, TypeMessage.NAO_LIDA));
			map.get(destinatario.getLogin()).get(emissor.getLogin()).addMessage(new MessageImpl(emissor, mensagem, destinatario, TypeMessage.NAO_LIDA));
			//System.out.println("Adicionado conversa privada");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public Set<String> getChaves(String login){
		return map.get(login).keySet();
	}
	
	public void adicionarConversaPublica(List<String> conectados, MessageImpl message) throws RemoteException {
		for(String c : conectados)
			map.get(c).get(GROUP).addMessage(message);
		//System.out.println("Adicionado mensagem em conversa p�blica");
	}
}