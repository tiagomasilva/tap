package chatrmi.server;

import java.rmi.RemoteException;
import java.util.Vector;

import chatrmi.model.MessageImpl;
import chatrmi.model.TypeMessage;

public class MessageListImpl implements IMessageList{
	
	private Vector<MessageImpl> historico, naoLidas;
	
	public MessageListImpl() {
		historico = new Vector<MessageImpl>();
		naoLidas = new Vector<MessageImpl>();
	}
	
	@Override
	public Vector<MessageImpl> getHistorico() throws RemoteException {
		return historico;
	}

	@Override
	public Vector<MessageImpl> getMessageNaoLidas() throws RemoteException {
		return naoLidas;
	}

	@Override
	public void changeType() throws RemoteException {
		naoLidas.forEach(message -> {
			message.setTypeMessage(TypeMessage.LIDA);
			historico.addElement(message);
		});
		/*for(MessageImpl message : naoLidas){
			message.setTypeMessage(TypeMessage.LIDA);
			historico.addElement(message);
		}*/
		naoLidas.removeAllElements();
	}

	@Override
	public void addMessage(MessageImpl message) throws RemoteException {
		naoLidas.addElement(message);
	}

	@Override
	public void deleteHistoric() throws RemoteException {
		naoLidas.removeAllElements();
		historico.removeAllElements();
	}

}