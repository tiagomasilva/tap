package chatrmi.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import chatrmi.cliente.IUser;
import chatrmi.model.MessageImpl;
import chatrmi.model.TypeMessage;

public class ServicoChatImpl implements IServicoChat {

	private Vector<IUser> conectados;
	private Conversa conversas;
	private static int PORT_IP_CLI = 2000;

	public ServicoChatImpl() {
		conectados = new Vector<IUser>();
		conversas = new Conversa();
	}

	@Override
	public int getConectados() throws RemoteException {
		conectados.forEach(System.out::println);
		return conectados.size();
	}

	@Override
	public void conectar(IUser u) throws RemoteException {
		conectados.add(u);
		conversas.registrarUsuario(u.getLogin()); // usar este m�todo
	}

	@Override
	public void enviarPublico(IUser userRemetente, String mensagem)
			throws RemoteException {
		conversas.adicionarConversaPublica(getLoginConectados(),
				new MessageImpl(mensagem, userRemetente, TypeMessage.NAO_LIDA));
	}

	@Override
	public void enviarPrivado(IUser emissor, String mensagem, IUser destinatario)
			throws RemoteException {
		conversas.adicionarConversaPrivada(emissor, mensagem, destinatario);
	}

	@Override
	public void desconectar(IUser u) throws RemoteException {
		conectados.remove(u);
	}

	@Override
	public Vector<MessageImpl> getMessagesPublicas(String login)
			throws RemoteException {
		return conversas.buscarConversaPublica(login);
	}

	@Override
	public Vector<MessageImpl> getMessagesPrivadas(String emissor,
			String destinatario) throws RemoteException {
		return conversas.buscarConversaPrivada(emissor, destinatario);
	}

	@Override
	public Vector<IUser> getUsersConectados() throws RemoteException {
		return conectados;
	}

	@Override
	public List<String> getLoginConectados() throws RemoteException {
		List<String> list = new ArrayList<String>();
		try {
			for (IUser u : conectados)
				list.add(u.getLogin());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int incPort() throws RemoteException {
		return PORT_IP_CLI += 1;
	}

	@Override
	public boolean atualizarMessagesPublicasNaoLidas(String login)
			throws RemoteException {
		return conversas.buscarConversaPublicaNaoLida(login).size() > 0;
	}

	@Override
	public boolean atualizarMessagesPrivadasNaoLidas(String login,
			String privado) throws RemoteException {
		if (conversas.existePrivado(login, privado))
			return conversas.buscarConversaPrivadaNaoLida(login, privado)
					.size() > 0;
		else
			return false;
	}

	@Override
	public StringBuilder mensagemPublica(String login) throws RemoteException {
		StringBuilder string = new StringBuilder();
		for (MessageImpl ms : conversas.buscarConversaPublicaNaoLida(login))
			string.append(ms.getUserEmissor().getLogin() + ": "
					+ ms.getMessage() + "\n");
		return string;
	}

	@Override
	public StringBuilder mensagemPrivada(String login, String privado)
			throws RemoteException {
		StringBuilder string = new StringBuilder();
		for (MessageImpl ms : conversas.buscarConversaPrivadaNaoLida(login,
				privado))
			string.append(ms.getUserEmissor().getLogin() + ": "
					+ ms.getMessage() + "\n");
		return string;
	}

	@Override
	public Vector<MessageImpl> getMessagesPrivadasNaoLidas(String emissor,
			String destinatario) throws RemoteException {
		return conversas.buscarConversaPrivadaNaoLida(emissor, destinatario);
	}

	@Override
	public void lerMensagem(String login) throws RemoteException {
		conversas.lerMessage(login);
	}

	@Override
	public void lerMensagemPrivada(String login, String privado)
			throws RemoteException {
		conversas.lerMessagePrivada(login, privado);
	}
}