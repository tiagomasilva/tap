package chatrmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

import chatrmi.model.MessageImpl;

public interface IMessageList extends Remote{
	
	public Vector<MessageImpl> getHistorico() throws RemoteException;
	public Vector<MessageImpl> getMessageNaoLidas() throws RemoteException;
	public void changeType() throws RemoteException;
	public void addMessage(MessageImpl message) throws RemoteException;
	public void deleteHistoric() throws RemoteException;

}