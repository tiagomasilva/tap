package com.comunidade.facade;

import com.comunidade.bo.GerenciarNotificacaoBO;
import com.comunidade.dao.entity.Notificacao;

public class FacadeNotificacao {

	public static boolean cadastrarNotificacao(Notificacao dotificacao) {
		return GerenciarNotificacaoBO.cadastrarNotificacao(dotificacao);
	}
	
	public static boolean atualizarNotificacao(Notificacao dotificacao) {
		return GerenciarNotificacaoBO.atualizarNotificacao(dotificacao);
	}
	
	public static boolean deletarNotificacao(Notificacao dotificacao) {
		return GerenciarNotificacaoBO.deletarNotificacao(dotificacao);
	}
	
}