package com.comunidade.facade;

import com.comunidade.bo.GerenciarDoadorBO;
import com.comunidade.dao.entity.Doador;

public class FacadeDoador {

	public static boolean cadastrarDoador(Doador doador) {
		return GerenciarDoadorBO.cadastrarDoador(doador);
	}
	
	public static boolean atualizarDoador(Doador doador) {
		return GerenciarDoadorBO.atualizarDoador(doador);
	}
	
	public static boolean desativarDoador(Doador doador) {
		return GerenciarDoadorBO.desativarDoador(doador);
	}
	
	public static boolean deletarDoador(Doador doador) {
		return GerenciarDoadorBO.deletarDoador(doador);
	}
	
	public static Doador logar(Doador doador) {
		return GerenciarDoadorBO.logar(doador);
	}
	
	public static Doador buscarPorCPF(String cpf) {
		return GerenciarDoadorBO.buscarDoadorPorCPF(cpf);
	}
	
	public static Doador recuperarSenhaPorEmail(String email) {
		return GerenciarDoadorBO.recuperarSenhaPorEmail(email);
	}
}
