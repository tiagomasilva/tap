package com.comunidade.facade;

import java.util.Date;
import java.util.List;

import com.comunidade.bo.GerenciarDoacaoBO;
import com.comunidade.dao.entity.Doacao;

public class FacadeDoacao {

	public static boolean cadastrarDoacao(Doacao doacao) {
		return GerenciarDoacaoBO.cadastrarDoacao(doacao);
	}
	
	public static boolean atualizarDoacao(Doacao doacao) {
		return GerenciarDoacaoBO.atualizarDoacao(doacao);
	}
	
//	public static boolean desativarDoacao(Doacao doacao) {
//		return GerenciarDoacaoBO.desativarDoacao(doacao);
//	}
	
	public static boolean deletarDoacao(Doacao doacao) {
		return GerenciarDoacaoBO.deletarDoacao(doacao);
	}
	
	public static List<Doacao> buscarPorCPF(String cpf) {
		return GerenciarDoacaoBO.buscarDoacoesPorCPF(cpf);
	}
	
	public static List<Doacao> buscarDoacoesNoIntervalo(Date a, Date b) {
		return GerenciarDoacaoBO.buscarDoacoesNoIntervalo(a, b);
	}
}
