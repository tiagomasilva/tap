package com.comunidade.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {
//	private static final String localProjeto = "C:/Users/Filipe/git/tap/Comunidade/";
	private static final String localProjeto = "C:/Users/Beatriz/git/tap/Comunidade/";
	private static Properties propertiesInfo;
	private static Properties propertiesArquivo;
	
	private static Properties getProp(String local) {
		Properties props = new Properties();
		FileInputStream file;
		try {
			file = new FileInputStream(localProjeto + local);
			props.load(file);
			return props;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getInfo(String key) {
		if (propertiesInfo == null) {
			propertiesInfo = getProp("src/main/java/com/comunidade/properties/info.properties");
		}
		return propertiesInfo.getProperty(key);
	}
	
	public static String getArquivo(String key) {
		if (propertiesArquivo == null) {
			propertiesArquivo = getProp("src/main/java/com/comunidade/properties/arquivo.properties");
		}
		return localProjeto + propertiesArquivo.getProperty(key);
	}

}
