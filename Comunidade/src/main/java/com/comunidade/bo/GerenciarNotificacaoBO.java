package com.comunidade.bo;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.comunidade.dao.entity.Notificacao;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.properties.PropertiesUtil;

public class GerenciarNotificacaoBO {

	public static boolean cadastrarNotificacao(Notificacao notificacaoIn) {
		if (notificacaoIn != null) {
			try {
				DaoFactory.getNotificacaoDao().insert(notificacaoIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " foi salvo com sucesso."));
				return true;

			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " Erro."));
				e.printStackTrace();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}

		return false;

	}

	public static boolean atualizarNotificacao(Notificacao notificacaoIn) {
		if (notificacaoIn != null) {
			try {
				DaoFactory.getNotificacaoDao().update(notificacaoIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " foi atualizado com sucesso."));

				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " Erro."));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}

		return false;
	}

	public static boolean deletarNotificacao(Notificacao notificacaoIn) {
		if (notificacaoIn != null) {
			try {
				DaoFactory.getDoadorDao().delete(notificacaoIn.getId());
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " foi Deletado com sucesso."));
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " Erro."));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		return false;
	}

}