package com.comunidade.bo;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.Deletado;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.dao.jinq.DoadorDao;
import com.comunidade.dao.jinq.SingletonDaoFactory;
import com.comunidade.properties.PropertiesUtil;
import com.comunidade.util.Util;
import com.comunidade.util.deletar.EmailHtml;
import com.comunidade.util.email.EmailComunidadeAbstractFactory;
import com.comunidade.util.email.EmailComunidadeTipo;
import com.comunidade.util.email.IEmailComunidade;

public class GerenciarDoadorBO {

	public static boolean cadastrarDoador(Doador doadorIn) {
		if (doadorIn != null) {
			try {
				if (!Util.validarCPF(doadorIn.getCpf())) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(doadorIn.getNome() + " CPF Invalido."));
					return false;
				}
				String nomesIguais = validaLogineCpfUnicos(doadorIn);

				if (!Util.isNull(nomesIguais)) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(nomesIguais));
					return false;
				}

				DaoFactory.getDoadorDao().insert(doadorIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(doadorIn.getNome() + " foi salvo com sucesso."));
				return true;
				
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(doadorIn.getNome() + " Erro."));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		
		return false;

	}

	public static boolean atualizarDoador(Doador doadorIn) {
		if (doadorIn != null) {
			try {
				if (!Util.validarCPF(doadorIn.getCpf())) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(doadorIn.getNome() + " CPF Invalido."));
					
					return false;
				}

				String nomesIguais = validaLogineCpfUnicos(doadorIn);

				if (!Util.isNull(nomesIguais)) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(nomesIguais));
					return false;
				}

				DaoFactory.getDoadorDao().update(doadorIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(doadorIn.getNome() + " foi atualizado com sucesso."));
				
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(doadorIn.getNome() + " Erro."));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		
		return true;
	}

	public static boolean deletarDoador(Doador doadorIn) {
		if (doadorIn != null) {
			try {
				DaoFactory.getDoadorDao().delete(doadorIn.getId());
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(doadorIn.getNome() + " foi Deletado com sucesso."));
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(doadorIn.getNome() + " Erro."));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		return false;
	}

	public static boolean desativarDoador(Doador doadorIn) {
		if (doadorIn != null) {
			try {
				doadorIn.setDeletado(Deletado.SIM.getNome());
				DaoFactory.getDoadorDao().update(doadorIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(doadorIn.getNome() + " foi Desativado com sucesso."));
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(doadorIn.getNome() + " Erro."));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		return false;
	}

	public static Doador logar(Doador doadorIn) {
		Doador doadorOut = new Doador();

		if (doadorIn != null) {
			try {
//				DoadorDao d = new DoadorDao();
//				doadorOut = d.getDoadorPorLoginSenha(doadorIn);
				doadorOut = SingletonDaoFactory.getDoadorDao().getDoadorPorLoginSenha(doadorIn);

			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(doadorIn.getNome() + " Erro."));
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(" Erro."));
		}

		if (doadorOut == null) {
			return null;

		} else {
			return doadorOut;
		}

	}

	public static Doador buscarDoadorPorCPF(String cpf) {
		Doador doadorOut = new Doador();

		if (!Util.isNull(cpf)) {
			try {
				DoadorDao d = new DoadorDao();
				doadorOut = d.getDoadorPorCPF(cpf);
				if (doadorOut.getId() != null) {
					return doadorOut;
				}

			} catch (NullPointerException e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doador.erro.nEncontrado")));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("doador.erro.vazio")));
		}

		return null;
	}

	
	public static String validaLogineCpfUnicos(Doador doadorIn) {
		StringBuilder nomesIguais = new StringBuilder();
		if (doadorIn != null) {
			try {
				DoadorDao d = new DoadorDao();
				for (Doador doadorAux : d.getDoadoresComCampoIguaisA(doadorIn)) {

					if (doadorIn.getId() == null || !doadorIn.getId().equals(doadorAux.getId())) {

						if (doadorIn.getLogin().equals(doadorAux.getLogin())) {
							nomesIguais.append("  Ja Existe Alguem com esse Login.  \n");
						}
						if (doadorIn.getCpf().equals(doadorAux.getCpf())) {
							nomesIguais.append("  Ja Existe Alguem com esse CPF. " + "\n");
						}

					}

				}

			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(doadorIn.getNome() + " Erro."));
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}

		if (Util.isNull(nomesIguais.toString())) {
			return null;

		} else {
			return nomesIguais.toString();
		}
	}
	
	public static Doador buscarDoadorPorEmail(String email) {
		Doador doadorOut = new Doador();

		if (!Util.isNull(email)) {
			try {
				doadorOut = SingletonDaoFactory.getDoadorDao().getDoadorPorEmail(email);

				if (doadorOut.getId() != null) {
					return doadorOut;
				}

			} catch (NullPointerException e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doador.erro.nEncontrado")));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("doador.erro.vazio")));
		}

		return null;
	}
	
	public static Doador recuperarSenhaPorEmail(String email) {
		Doador doadorOut = new Doador();

		if (!Util.isNull(email)) {
			try {
				doadorOut = buscarDoadorPorEmail(email);

				if (doadorOut.getId() != null) {
					Map<String, Doador> doadorTemp = new HashMap<String,Doador>();
					doadorTemp.put(doadorOut.getEmail(), doadorOut);
					IEmailComunidade emailComunidade = EmailComunidadeAbstractFactory.getInstancia(doadorTemp, EmailComunidadeTipo.RECUPERAR_SENHA, true);
					emailComunidade.send();
					return doadorOut;
				}

			} catch (NullPointerException e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doador.erro.nEncontrado")));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("doador.erro.vazio")));
		}

		return null;
	}

}