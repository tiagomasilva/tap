package com.comunidade.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.comunidade.dao.entity.Doacao;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.dao.jinq.DoacaoDao;
import com.comunidade.dao.jinq.DoadorDao;
import com.comunidade.dao.jinq.SingletonDaoFactory;
import com.comunidade.properties.PropertiesUtil;
import com.comunidade.util.Util;

public class GerenciarDoacaoBO {

	public static boolean cadastrarDoacao(Doacao doacaoIn) {
		if (doacaoIn != null) {
			try {

				DaoFactory.getDoacaoDao().insert(doacaoIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.info.salvoSucesso")));
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		return false;
	}

	public static boolean atualizarDoacao(Doacao doacaoIn) {
		if (doacaoIn != null) {
			try {
				DaoFactory.getDoacaoDao().update(doacaoIn);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.info.atualizadoSucesso")));
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		return false;
	}

	public static boolean deletarDoacao(Doacao doacaoIn) {
		if (doacaoIn != null) {
			try {
				DaoFactory.getDoacaoDao().delete(doacaoIn.getId());
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.info.deletSucesso")));
				return true;
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
		return false;
	}

	public static List<Doacao> buscarDoacoesPorCPF(String cpf) {
		List<Doacao> doacoesOut;

		if (!Util.isNull(cpf)) {
			try {
				doacoesOut = SingletonDaoFactory.getDoacaoDao().getDoacaoPorCPF(cpf);

				if (doacoesOut.size() > 0) {
					return doacoesOut;
				}

			} catch (NullPointerException e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.erro.nEncontrado")));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.erro.erro")));
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("doacao.erro.vazio")));
		}

		return null;

	}
	
	public static List<Doacao> buscarDoacoesNoIntervalo(Date a, Date b) {
		List<Doacao> doacoesOut = new ArrayList<Doacao>();
		if((a!=null) && (b!=null) && (a.after(b))){
			return doacoesOut;
		}
			try {
				doacoesOut = SingletonDaoFactory.getDoacaoDao().getTodasDoacoesNoIntervalo(a, b);

				if (doacoesOut.size() > 0) {
					return doacoesOut;
				}

			} catch (NullPointerException e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.erro.nEncontrado")));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(PropertiesUtil.getInfo("doacao.erro.erro")));
			}

			return doacoesOut;
	}
	
}