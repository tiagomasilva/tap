package com.comunidade.mb;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import com.comunidade.dao.entity.Doacao;
import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.Sexo;
import com.comunidade.dao.enums.StatusDoacao;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.facade.FacadeDoacao;
import com.comunidade.facade.FacadeDoador;
import com.comunidade.util.Util;

@SuppressWarnings("serial")
@ManagedBean
public class DoacaoMB implements Serializable {
	private Doacao doacao;
	private Doacao doacaoAux;
	private Doador doador;
	private Doador doadorAux;
	private List<Doacao> myDoacoes;
	private List<Doacao> doacoes;
	private String nomeDoadorCPF;

	public DoacaoMB() {
		doadorAux = (Doador) SessionContext.getInstance().getAttribute("USUARIO");
		if (doadorAux == null) {
			doadorAux = new Doador();
		}
		doador = new Doador();
		doacao = new Doacao();
		doacaoAux = new Doacao();
		doacoes = new ArrayList<Doacao>();
	}

	public void salvar(ActionEvent action) {
		doacao = doacaoAux;

		try {

			Doador d = FacadeDoador.buscarPorCPF(getDoacaoAux().getCpf());
			if (!Util.isNull(d.getNome())) {
				doacao.setDoador(d);
			} else {
				findDoadorPorCPF(null);
				return;
			}

			doacao.setDate(Date.from(Instant.now()));
			boolean cadastrou = FacadeDoacao.cadastrarDoacao(doacao);

			if (cadastrou) {
				Date date = Date.from(Instant.now());
				addDias(doacao.getStatusDoacao(), doacao.getDoador());
				d.setProxDoacao(date);

				doacao = new Doacao();
				doacaoAux = new Doacao();
			}
		} catch (Exception e) {
			e.printStackTrace();
			findDoadorPorCPF(null);
		}

	}

	public Date addDias(String statusDoacaoIn, Doador doadorIn) {
		Calendar calendar = Calendar.getInstance();
		int dias = 0;
		StatusDoacao statusDoacaoAux = StatusDoacao.valueOf(statusDoacaoIn);

		switch (statusDoacaoAux) {
		case OK:
			if (Sexo.M.getNome() == doadorIn.getSexo()) {
				dias = 60;
			} else {
				dias = 90;
			}
			break;
		case IMPEDIDO:
			dias = 15;
			break;
		case BLOQUEADO:
			dias = 100000;
			break;

		default:
			dias = 0;
			break;
		}

		calendar.add(Calendar.DATE, dias);
		return Date.from(calendar.toInstant());
	}

	public void findDoadorPorCPF(ActionEvent action) {
		try {
			Doador d = FacadeDoador.buscarPorCPF(getDoacaoAux().getCpf());
			if (!Util.isNull(d.getNome())) {
				setNomeDoadorCPF(d.getNome());
			} else {
				setNomeDoadorCPF("Doador n�o Encontrado");
			}
		} catch (NullPointerException e) {
			setNomeDoadorCPF("Doador n�o Encontrado");
		} catch (Exception e) {
			setNomeDoadorCPF("Doador n�o Encontrado");
		}
	}

	public void findMyDoacoes(ActionEvent action) {
		FacadeDoacao.buscarPorCPF(getDoacaoAux().getCpf());
		this.myDoacoes = FacadeDoacao.buscarPorCPF(getDoacaoAux().getCpf());
		;
	}

	public void findAllDoacoes(ActionEvent action) {
		this.doacoes = DaoFactory.getDoacaoDao().retrieve();
	}

	// Getters e Setters

	public Doacao getDoacao() {
		return doacao;
	}

	public void setDoacao(Doacao doacao) {
		this.doacao = doacao;
	}

	public Doacao getDoacaoAux() {
		return doacaoAux;
	}

	public void setDoacaoAux(Doacao doacaoAux) {
		this.doacaoAux = doacaoAux;
	}

	public Doador getDoador() {
		return doadorAux;
	}

	public void setDoador(Doador pessoa) {
		this.doadorAux = pessoa;
	}

	public Doador getDoadorAux() {
		return doadorAux;
	}

	public void setDoadorAux(Doador doadorAux) {
		this.doadorAux = doadorAux;
	}

	public String getNomeDoadorCPF() {
		return nomeDoadorCPF;
	}

	public void setNomeDoadorCPF(String nomeDoadorCPF) {
		this.nomeDoadorCPF = nomeDoadorCPF;
	}

	public List<Doacao> getDoacoes() {
		return doacoes;
	}

	public void setDoacoes(List<Doacao> doacoes) {
		this.doacoes = doacoes;
	}

	public List<Doacao> getMyDoacoes() {
		return myDoacoes;
	}

	public void setMyDoacoes(List<Doacao> myDoacoes) {
		this.myDoacoes = myDoacoes;
	}

	public StatusDoacao[] getStatusDoacao() {
		return StatusDoacao.values();
	}
}