package com.comunidade.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.entity.Notificacao;
import com.comunidade.dao.enums.Sexo;
import com.comunidade.dao.enums.TipoSangue;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.dao.jinq.SingletonDaoFactory;
import com.comunidade.properties.PropertiesUtil;
import com.comunidade.util.deletar.EmailHtml;
import com.comunidade.util.deletar.EmailText;
import com.comunidade.util.deletar.Mensagem;
import com.comunidade.util.email.EmailComunidadeAbstractFactory;
import com.comunidade.util.email.EmailComunidadeMult;
import com.comunidade.util.email.EmailComunidadeTipo;
import com.comunidade.util.email.IEmailComunidade;

@SuppressWarnings("serial")
@ManagedBean
public class NotificacaoMB implements Serializable {
	private Notificacao notificacao;
	private Notificacao notificacaoAux;
	private List<Notificacao> notoficacoes;

	// Swich
	private boolean aP;
	private boolean aN;
	private boolean bP;
	private boolean bN;
	private boolean abP;
	private boolean abN;
	private boolean oP;
	private boolean oN;
	private boolean raro;
	// fim

	private Doador doador;
	private Doador doadorAux;
	private List<Doador> doadores;

	public NotificacaoMB() {
		notificacao = new Notificacao();
		notificacaoAux = new Notificacao();
		notoficacoes = new ArrayList<Notificacao>();

		doadorAux = (Doador) SessionContext.getInstance().getAttribute("USUARIO");
		if (doadorAux == null) {
			doadorAux = new Doador();
		}
		doador = new Doador();
		doadores = new ArrayList<Doador>();
	}

	public Doador getDoador() {
		return doadorAux;
	}

	public void setDoador(Doador pessoa) {
		this.doadorAux = pessoa;
	}

	public Doador getDoadorAux() {
		return doadorAux;
	}

	public void setDoadorAux(Doador doadorAux) {
		this.doadorAux = doadorAux;
	}

	public List<Doador> getDoadores() {
		return doadores;
	}

	public void setDoadores(List<Doador> doadores) {
		this.doadores = doadores;
	}

	public TipoSangue[] getTipoSangue() {
		return TipoSangue.values();
	}

	public Notificacao getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(Notificacao notificacao) {
		this.notificacao = notificacao;
	}

	public Notificacao getNotificacaoAux() {
		return notificacaoAux;
	}

	public void setNotificacaoAux(Notificacao notificacaoAux) {
		this.notificacaoAux = notificacaoAux;
	}

	public Sexo[] getSexo() {
		return Sexo.values();
	}

	private void setSanguesTela() {
		setaP((notificacaoAux.getaP() == 1));
		setaN((notificacaoAux.getaN() == 1));
		setbP((notificacaoAux.getbP() == 1));
		setbN((notificacaoAux.getbN() == 1));
		setAbP((notificacaoAux.getAbP() == 1));
		setAbN((notificacaoAux.getAbN() == 1));
		setoP((notificacaoAux.getoP() == 1));
		setoN((notificacaoAux.getoN() == 1));
		setRaro((notificacaoAux.getRaro() == 1));
	}

	private void getSanguesTela() {
		notificacaoAux.setaP((isaP()) ? 1 : 0);
		notificacaoAux.setaN((isaN()) ? 1 : 0);
		notificacaoAux.setbP((isbP()) ? 1 : 0);
		notificacaoAux.setbN((isbN()) ? 1 : 0);
		notificacaoAux.setAbP((isAbP()) ? 1 : 0);
		notificacaoAux.setAbN((isAbP()) ? 1 : 0);
		notificacaoAux.setoP((isoP()) ? 1 : 0);
		notificacaoAux.setoN((isoN()) ? 1 : 0);
		notificacaoAux.setRaro((isRaro()) ? 1 : 0);
	}

	public void salvar(ActionEvent action) {
		getSanguesTela();
		notificacao = notificacaoAux;

		if (notificacao != null) {
			try {
				DaoFactory.getNotificacaoDao().insert(notificacao);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacao.getTitulo() + " foi salvo com sucesso."));

				// Enviar Automatico
				enviarNotificaçãoViaEmail(notificacao);

			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacao.getTitulo() + " Erro."));
				e.printStackTrace();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}

	}

	public void enviarNotificaçãoViaEmail(Notificacao notificacaoIn) {

		if (notificacaoIn != null) {
			try {
		
					Map<String, Doador> emailComDoador = SingletonDaoFactory.getDoadorDao()
							.getEmailsDoadoresPersonalizada(notificacaoIn);
					
					IEmailComunidade emailComunidade = EmailComunidadeAbstractFactory.getInstancia(emailComDoador, EmailComunidadeTipo.NOTIFICACAO_BB_SEM_GRAFICO, true);
					emailComunidade.send();
					
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " Enviado com  Sucesso."));

			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacaoIn.getTitulo() + " Erro."));
				e.printStackTrace();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}

	}

	public void atualizar(ActionEvent action) {
		setSanguesTela();
		notificacao = notificacaoAux;

		if (notificacao != null) {
			try {
				DaoFactory.getNotificacaoDao().update(notificacao);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacao.getTitulo() + " foi Atualizado com sucesso."));

			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(notificacao.getTitulo() + " Erro."));
				e.printStackTrace();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(PropertiesUtil.getInfo("global.erro.erro")));
		}
	}

	public void BuscarNotificacoes(ActionEvent action) {
		this.notoficacoes = DaoFactory.getNotificacaoDao().retrieve();
	}

	public boolean isaP() {
		return aP;
	}

	public void setaP(boolean aP) {
		this.aP = aP;
	}

	public boolean isaN() {
		return aN;
	}

	public void setaN(boolean aN) {
		this.aN = aN;
	}

	public boolean isbP() {
		return bP;
	}

	public void setbP(boolean bP) {
		this.bP = bP;
	}

	public boolean isbN() {
		return bN;
	}

	public void setbN(boolean bN) {
		this.bN = bN;
	}

	public boolean isAbP() {
		return abP;
	}

	public void setAbP(boolean abP) {
		this.abP = abP;
	}

	public boolean isAbN() {
		return abN;
	}

	public void setAbN(boolean abN) {
		this.abN = abN;
	}

	public boolean isoP() {
		return oP;
	}

	public void setoP(boolean oP) {
		this.oP = oP;
	}

	public boolean isoN() {
		return oN;
	}

	public void setoN(boolean oN) {
		this.oN = oN;
	}

	public boolean isRaro() {
		return raro;
	}

	public void setRaro(boolean raro) {
		this.raro = raro;
	}

}