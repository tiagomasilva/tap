package com.comunidade.mb;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import com.comunidade.dao.entity.Doacao;
import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.StatusDoacao;
import com.comunidade.dao.jinq.SingletonDaoFactory;
import com.comunidade.facade.FacadeDoacao;
import com.comunidade.facade.FacadeDoador;
import com.comunidade.util.Util;

@SuppressWarnings("serial")
@ManagedBean
public class HistoricoMB implements Serializable {
	private Doacao doacao;
	private Doacao doacaoAux;
	private Doador doador;
	private Doador doadorAux;
	private List<Doacao> myDoacoes;
	private Date inicio;
	private Date fim;
	private List<Doacao> allDoacoes;

	public HistoricoMB() {
		doadorAux = (Doador) SessionContext.getInstance().getAttribute("USUARIO");
		if (doadorAux == null) {
			doadorAux = new Doador();
		}
		doador = new Doador();
		doacao = new Doacao();
		doacaoAux = new Doacao();
	}

	public void salvar(ActionEvent action) {
		doacao = doacaoAux;

		try {
			Doador d = FacadeDoador.buscarPorCPF(getDoacaoAux().getCpf());
			if (!Util.isNull(d.getNome())) {
				doacao.setDoador(d);
			}

			boolean cadastrou = FacadeDoacao.cadastrarDoacao(doacao);

			if (cadastrou) {
				doacao = new Doacao();
				doacaoAux = new Doacao();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void findMyDoacoes(ActionEvent action) {
		try {
			this.myDoacoes = FacadeDoacao.buscarPorCPF(getDoadorAux().getCpf());

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void findAllMyDoacoes(ActionEvent action) {
		try {
			this.setAllDoacoes(FacadeDoacao.buscarDoacoesNoIntervalo(inicio, fim));

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// Getters e Setters

	public Doacao getDoacao() {
		return doacao;
	}

	public void setDoacao(Doacao doacao) {
		this.doacao = doacao;
	}

	public Doacao getDoacaoAux() {
		return doacaoAux;
	}

	public void setDoacaoAux(Doacao doacaoAux) {
		this.doacaoAux = doacaoAux;
	}

	public Doador getDoador() {
		return doadorAux;
	}

	public void setDoador(Doador pessoa) {
		this.doadorAux = pessoa;
	}

	public Doador getDoadorAux() {
		return doadorAux;
	}

	public void setDoadorAux(Doador doadorAux) {
		this.doadorAux = doadorAux;
	}

	public List<Doacao> getMyDoacoes() {
		return myDoacoes;
	}

	public void setMyDoacoes(List<Doacao> myDoacoes) {
		this.myDoacoes = myDoacoes;
	}

	public StatusDoacao[] getStatusDoacao() {
		return StatusDoacao.values();
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}
	
	public List<Doacao> getAllDoacoes() {
		return allDoacoes;
	}
	
	public void setAllDoacoes(List<Doacao> allDoacoes) {
		this.allDoacoes = allDoacoes;
	}
}