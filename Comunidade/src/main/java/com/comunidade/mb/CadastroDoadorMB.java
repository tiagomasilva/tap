package com.comunidade.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.Deletado;
import com.comunidade.dao.enums.Sexo;
import com.comunidade.dao.enums.StatusDoador;
import com.comunidade.dao.enums.TipoSangue;
import com.comunidade.dao.enums.TipoUsuario;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.facade.FacadeDoador;
import com.comunidade.util.Util;

@SuppressWarnings("serial")
@ManagedBean
public class CadastroDoadorMB implements Serializable {

	private Doador doador;
	private Doador doadorAux;
	private List<Doador> doadores;

	public CadastroDoadorMB() {
		doadorAux = (Doador) SessionContext.getInstance().getAttribute("USUARIO");
		if (doadorAux == null) {
			doadorAux = new Doador();
		}
		doador = new Doador();
		doadores = new ArrayList<Doador>();
	}

	public Doador getDoador() {
		return doadorAux;
	}

	public void setDoador(Doador pessoa) {
		this.doadorAux = pessoa;
	}

	public Doador getDoadorAux() {
		return doadorAux;
	}

	public void setDoadorAux(Doador doadorAux) {
		this.doadorAux = doadorAux;
	}

	public List<Doador> getDoadores() {
		return doadores;
	}

	public void setDoadores(List<Doador> doadores) {
		this.doadores = doadores;
	}

	public TipoSangue[] getTipoSangue() {
		return TipoSangue.values();
	}
	
	public Sexo[] getSexo() {
		return Sexo.values();
	}

	public void salvar(ActionEvent action) {
		doador = doadorAux;
		doador.setStatusDoador(StatusDoador.PASSIVO);
		doador.setTipoUsuario(TipoUsuario.DOADOR);
		doador.setDeletado(Deletado.NAO.getNome());

		try {
			if (FacadeDoador.cadastrarDoador(doador)) {
				doador = new Doador();
				doadorAux = new Doador();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void atualizarDoador(ActionEvent action) {
		doador = doadorAux;
		FacadeDoador.atualizarDoador(doador);
	}

	public void atualizar(ActionEvent action) {
		this.doadores = DaoFactory.getDoadorDao().retrieve();
	}

}
