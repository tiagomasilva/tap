package com.comunidade.mb;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.TipoUsuario;
import com.comunidade.facade.FacadeDoador;
import com.comunidade.properties.PropertiesUtil;

@SuppressWarnings("serial")
@ManagedBean
public class LoginMB implements Serializable {
	private Doador doador;
	private Doador doadorAux;

	public LoginMB() {
		doador = new Doador();
		doadorAux = new Doador();
	}

	public <HttpSession> void logar(ActionEvent actionEvent) {
		setDoador(getDoadorAux());
		setDoador(FacadeDoador.logar(getDoador()));
		if (getDoador() != null) {
			setDoadorAux(getDoador());

			SessionContext.getInstance().delAttribute("USUARIO");
			SessionContext.getInstance().setAttribute("USUARIO", getDoadorAux());

			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(" Logado com sucesso."));
			try {
//				if (getDoadorAux().getTipoUsuario().getNome().equals(TipoUsuario.ADM.getNome()))
					FacesContext.getCurrentInstance().getExternalContext().redirect("logado.xhtml");
//				else
//					FacesContext.getCurrentInstance().getExternalContext().redirect("loguser.xhtml");

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(" Usuario/Senha Errada."));
			doador = null;
//			try {
//			
//				FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
//
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
	}
	
	public boolean isLogado() {
		doador = (Doador) SessionContext.getInstance().getAttribute("USUARIO");
		if (doador != null) {
			return true;
		}
		return false;
	}
	
	public boolean isRedirecionar() {
		
		if (isLogado()) {
			return true;
		}
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	} 
	
	public boolean isADM() {
		doador = (Doador) SessionContext.getInstance().getAttribute("USUARIO");
		if (doador != null) {
			return doador.getTipoUsuario().getNome().equals(TipoUsuario.ADM.getNome());
		}
		return false;
	}

	public <HttpSession> void logout(ActionEvent actionEvent) {
		doadorAux = null;
		doador = null;
		try {
			SessionContext.getInstance().delAttribute("USUARIO");
			SessionContext.getInstance().encerrarSessao();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(" Deslogado com sucesso."));
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(" Erro ao Deslogar."));
		}
	}
	
	public <HttpSession> void recuperarSenha(ActionEvent actionEvent) {
		try {
			FacadeDoador.recuperarSenhaPorEmail(doadorAux.getEmail());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Email de Recuperar Senha Enviado com Sucesso!"));
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(" Erro."));
		}
	}

	public Doador getDoador() {
		return doador;
	}

	public void setDoador(Doador doador) {
		this.doador = doador;
	}

	public Doador getDoadorAux() {
		return doadorAux;
	}

	public void setDoadorAux(Doador doadorAux) {
		this.doadorAux = doadorAux;
	}

}