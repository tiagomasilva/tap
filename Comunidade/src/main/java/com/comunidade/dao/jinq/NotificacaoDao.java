package com.comunidade.dao.jinq;

import com.comunidade.dao.entity.Notificacao;
import com.comunidade.dao.idao.INotificacaoDao;

public class NotificacaoDao extends GenericDaoJinq<Notificacao> implements INotificacaoDao {

	public NotificacaoDao() {
		super(Notificacao.class);
	}

}