package com.comunidade.dao.jinq;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.comunidade.dao.entity.Doacao;
import com.comunidade.dao.idao.IDao;
import com.comunidade.dao.idao.IDoacaoDao;

public class DoacaoDao extends GenericDaoJinq<Doacao> implements IDoacaoDao {

	public DoacaoDao() {
		super(Doacao.class);
	}

	public List<Doacao> getDoacaoPorCPF(String cpf) {
		try {

			return getStream().select(doacao -> doacao).
					where(doacao -> doacao.getCpf().equals(cpf)).
					collect(Collectors.toList());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Doacao> getDoacaoPorCPFnoIntervalo(String cpf, Date aIn, Date bIn) {
		final Date a = aIn;
		final Date b = bIn;

		try {

			return getStream().select(doacao -> doacao).where(
					doacao -> doacao.getCpf().equals(cpf) && doacao.getDate().after(a) && doacao.getDate().before(b))
					.collect(Collectors.toList());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Doacao> getTodasDoacoesNoIntervalo(Date aIn, Date bIn) {
		final Date a = aIn;
		final Date b = bIn;

		try {

			return getStream().select(doacao -> doacao).
					where(doacao -> ( a==null || !doacao.getDate().before(a) ) && (b == null || !doacao.getDate().after(b) )).
					collect(Collectors.toList());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}