package com.comunidade.dao.jinq;

import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jinq.orm.stream.JinqStream;
import org.jinq.tuples.Pair;

import com.comunidade.dao.entity.Doacao;
import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.entity.Notificacao;
import com.comunidade.dao.enums.TipoSangue;
import com.comunidade.dao.idao.IDoadorDao;

public class DoadorDao extends GenericDaoJinq<Doador> implements IDoadorDao {

	public DoadorDao() {
		super(Doador.class);
	}

	public Doador getDoadorPorLoginSenha(Doador doadorIn) {
		final String login = doadorIn.getLogin();
		final String senha = doadorIn.getSenha();

		try {

			return getStream().select(x -> x).where(y -> y.getLogin().equals(login) && y.getSenha().equals(senha))
					.findOne().get();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Doador getDoadorPorCPF(final String cpf) {

		try {

			return getStream().select(x -> x).where(y -> y.getCpf().equals(cpf)).findOne().get();

		} catch (java.util.NoSuchElementException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} catch (Error e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Doador> getDoadoresComCampoIguaisA(Doador doadorIn) {
		final String login = doadorIn.getLogin();
		final String cpf = doadorIn.getCpf();

		try {

			return getStream().select(x -> x).where(y -> y.getLogin().equals(login) || y.getCpf().equals(cpf)).toList();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Doacao> getDoacaoPorCPFDoador(String cpf) {
		try {

			return getStream().select(doador -> doador).where(y -> y.getCpf().equals(cpf))
					.join(doador -> JinqStream.from(doador.getDoacoes())).select(doacoes -> doacoes.getTwo())
					.collect(Collectors.toList());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getEmailsDoadores(Notificacao notificacaoIn) {
		
		Set<String> tpSangue = new HashSet();
		tpSangue.add((notificacaoIn.getaP() == 1) ? TipoSangue.A_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getaN() == 1) ? TipoSangue.A_NEGATIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getbP() == 1) ? TipoSangue.B_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getbN() == 1) ? TipoSangue.B_NEGATIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getAbP() == 1) ? TipoSangue.AB_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getAbN() == 1) ? TipoSangue.AB_NEGATIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getoP() == 1) ? TipoSangue.O_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getoN() == 1) ? TipoSangue.O_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getRaro() == 1) ? TipoSangue.RARO.getNome() : null);
		
		final Date dataLimite = Date.from(Instant.now());
		
		try {

			return getStream().
					select(x -> x).
					where(y -> tpSangue.contains(y.getTipoSangue()) && !y.getProxDoacao().after(dataLimite)).
					select(x -> x.getEmail())
					.toList();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
public  Map<String, Doador> getEmailsDoadoresPersonalizada(Notificacao notificacaoIn) {
		
		Set<String> tpSangue = new HashSet();
		tpSangue.add((notificacaoIn.getaP() == 1) ? TipoSangue.A_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getaN() == 1) ? TipoSangue.A_NEGATIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getbP() == 1) ? TipoSangue.B_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getbN() == 1) ? TipoSangue.B_NEGATIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getAbP() == 1) ? TipoSangue.AB_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getAbN() == 1) ? TipoSangue.AB_NEGATIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getoP() == 1) ? TipoSangue.O_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getoN() == 1) ? TipoSangue.O_POSITIVO.getNome() : null);
		tpSangue.add((notificacaoIn.getRaro() == 1) ? TipoSangue.RARO.getNome() : null);
		
		final Date dataLimite = Date.from(Instant.now());
		
		try {

			return getStream().
					select(x -> x).
					where(y -> tpSangue.contains(y.getTipoSangue()) && !y.getProxDoacao().after(dataLimite)).
					select(x -> new Pair<>(x.getEmail(), x)).
					collect(Collectors.toMap(Pair::getOne,Pair::getTwo));
					
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
public Doador getDoadorPorEmail(final String email) {

	try {

		return getStream().select(x -> x).
				where(y -> y.getEmail().equals(email)).
				findOne().
				get();

	} catch (java.util.NoSuchElementException e) {
		e.printStackTrace();
		return null;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	} catch (Error e) {
		e.printStackTrace();
		return null;
	}
}

}
