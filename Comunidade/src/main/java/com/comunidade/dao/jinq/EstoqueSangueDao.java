package com.comunidade.dao.jinq;

import com.comunidade.dao.entity.EstoqueSangue;
import com.comunidade.dao.idao.IEstoqueSangueDao;

public class EstoqueSangueDao extends GenericDaoJinq<EstoqueSangue> implements IEstoqueSangueDao {

	public EstoqueSangueDao() {
		super(EstoqueSangue.class);
	}

}