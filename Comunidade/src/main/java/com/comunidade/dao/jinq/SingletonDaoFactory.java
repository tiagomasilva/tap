package com.comunidade.dao.jinq;

public class SingletonDaoFactory {

	private static DoadorDao doadorDao;
	private static DoacaoDao doacaoDao;
	private static NotificacaoDao notificacaoDao;

	public static DoadorDao getDoadorDao() {
		if (doadorDao == null) {
			doadorDao = new DoadorDao();
		}
		return doadorDao;
	}

	public static DoacaoDao getDoacaoDao() {
		if (doacaoDao == null) {
			doacaoDao = new DoacaoDao();
		}
		return doacaoDao;
	}

	public static NotificacaoDao getNotificacaoDao() {
		if (notificacaoDao == null) {
			notificacaoDao = new NotificacaoDao();
		}
		return notificacaoDao;
	}

}
