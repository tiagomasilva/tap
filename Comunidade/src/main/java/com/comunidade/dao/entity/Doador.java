package com.comunidade.dao.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comunidade.dao.enums.StatusDoador;
import com.comunidade.dao.enums.TipoUsuario;

@Entity
@Table(name = "doador")
public class Doador extends BaseEntity implements Cloneable {
	
	private String login;
	private String senha;
	private String email;
	private String lembraSenha;
	private TipoUsuario tipoUsuario;
	private String nome;
	private java.util.Date dataNasci;
	private String sexo;
	private String cpf;
	private String tipoSangue;
	private StatusDoador StatusDoador;
	private List<Doacao> doacoes;
	private java.util.Date proxDoacao;
	private String deletado;

	public Doador() {

	}
	
	@Column(length = 75, nullable = false, unique = true)
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	@Column(length = 20, nullable = false, unique = false)
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Column(length = 100, nullable = false)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	@Column(nullable = true)
	public String getLembraSenha() {
		return lembraSenha;
	}
	
	public void setLembraSenha(String lembraSenha) {
		this.lembraSenha = lembraSenha;
	}
	
	@Enumerated(EnumType.STRING)
	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}
	
	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	@Column(length = 150, nullable = false, unique = false)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Temporal(value = TemporalType.TIMESTAMP)
	public java.util.Date getDataNasci() {
		return dataNasci;
	}

	public void setDataNasci(java.util.Date dataNasci) {
		this.dataNasci = dataNasci;
	}
	
	@Column(nullable = true)
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	@Column(length = 14, nullable = false, unique = true)
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	@Column(nullable = true)
	public String getTipoSangue() {
		return tipoSangue;
	}

	public void setTipoSangue(String tipoSangue) {
		this.tipoSangue = tipoSangue;
	}

	@Enumerated(EnumType.STRING)
	public StatusDoador getStatusDoador() {
		return StatusDoador;
	}

	public void setStatusDoador(StatusDoador statusDoador) {
		StatusDoador = statusDoador;
	}
	
	@OneToMany(mappedBy = "doador")
	public List<Doacao> getDoacoes() {
		return doacoes;
	}
	
	public void setDoacoes(List<Doacao> doacoes) {
		this.doacoes = doacoes;
	}
	
	@Temporal(value = TemporalType.TIMESTAMP)
	public java.util.Date getProxDoacao() {
		return proxDoacao;
	}

	public void setProxDoacao(java.util.Date proxDoacao) {
		this.proxDoacao = proxDoacao;
	}
	
	@Column(length = 3, nullable = false)
	public String getDeletado() {
		return deletado;
	}

	public void setDeletado(String deletado) {
		this.deletado = deletado;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public Doador clone() {
		try {
			return (Doador) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.getMessage());
		}
	}
}