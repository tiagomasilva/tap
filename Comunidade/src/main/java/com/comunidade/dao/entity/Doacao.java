package com.comunidade.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "doacao")
public class Doacao extends BaseEntity implements Cloneable {
	private Doador doador;
	private String cpf;
	private String codigo;
	private java.util.Date date;
	private String Observacao;
	private String statusDoacao;

	public Doacao() {

	}

	@ManyToOne
	@JoinColumn(name="doador_id", nullable = true, updatable=true)
	public Doador getDoador() {
		return doador;
	}

	public void setDoador(Doador doador) {
		this.doador = doador;
	}
	
	@Column(length = 14, nullable = false)
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	@Column(length = 8, nullable = true, unique = false)
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	@Column(nullable = true)
	public String getObservacao() {
		return Observacao;
	}

	public void setObservacao(String observacao) {
		Observacao = observacao;
	}

	@Column(nullable = false)
	public String getStatusDoacao() {
		return statusDoacao;
	}

	public void setStatusDoacao(String statusDoacao) {
		this.statusDoacao = statusDoacao;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public Doacao clone() {
		try {
			return (Doacao) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.getMessage());
		}
	}

}