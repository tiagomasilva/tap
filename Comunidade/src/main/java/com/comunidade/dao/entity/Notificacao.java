package com.comunidade.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "notificacao")
public class Notificacao extends BaseEntity implements Cloneable {
	private String titulo;
	private String conteudo;
	private int aP;
	private int aN;
	private int bP;
	private int bN;
	private int abP;
	private int abN;
	private int oP;
	private int oN;
	private int raro;

	public Notificacao() {

	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	@Column(length = 1)
	public int getaP() {
		return aP;
	}

	public void setaP(int aP) {
		this.aP = aP;
	}

	@Column(length = 1)
	public int getaN() {
		return aN;
	}

	public void setaN(int aN) {
		this.aN = aN;
	}

	@Column(length = 1)
	public int getbP() {
		return bP;
	}

	public void setbP(int bP) {
		this.bP = bP;
	}

	@Column(length = 1)
	public int getbN() {
		return bN;
	}

	public void setbN(int bN) {
		this.bN = bN;
	}

	@Column(length = 1)
	public int getAbP() {
		return abP;
	}

	public void setAbP(int abP) {
		this.abP = abP;
	}

	@Column(length = 1)
	public int getAbN() {
		return abN;
	}

	public void setAbN(int abN) {
		this.abN = abN;
	}

	@Column(length = 1)
	public int getoP() {
		return oP;
	}

	public void setoP(int oP) {
		this.oP = oP;
	}

	@Column(length = 1, name = "oNeg")
	public int getoN() {
		return oN;
	}

	public void setoN(int oN) {
		this.oN = oN;
	}

	@Column(length = 1)
	public int getRaro() {
		return raro;
	}

	public void setRaro(int raro) {
		this.raro = raro;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public Notificacao clone() {
		try {
			return (Notificacao) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.getMessage());
		}
	}

}
