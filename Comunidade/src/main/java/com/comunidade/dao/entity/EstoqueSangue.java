package com.comunidade.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "estoqueSangue")
public class EstoqueSangue extends BaseEntity implements Cloneable {
	private String tipoSangue;
	private int quantidade;

	public EstoqueSangue() {

	}
	
	@Column(nullable = true)
	public String getTipoSangue() {
		return tipoSangue;
	}

	public void setTipoSangue(String tipoSangue) {
		this.tipoSangue = tipoSangue;
	}

	@Column(length = 3)
	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public EstoqueSangue clone() {
		try {
			return (EstoqueSangue) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.getMessage());
		}
	}
}