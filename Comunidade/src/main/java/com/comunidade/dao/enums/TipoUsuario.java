package com.comunidade.dao.enums;

public enum TipoUsuario {
	DOADOR("DOADOR",0),
	ADM("ADM",1);

	private String nome;
	private int cod;

	TipoUsuario(String nome, int cod) {
		this.nome = nome;
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}
}
