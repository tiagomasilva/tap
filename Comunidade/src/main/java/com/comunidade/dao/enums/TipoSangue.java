package com.comunidade.dao.enums;

public enum TipoSangue {
	A_POSITIVO("A Positivo",1),
	A_NEGATIVO("A Negativo",2),
	B_POSITIVO("B Positivo",3),
	B_NEGATIVO("B Negativo",4),
	AB_POSITIVO("AB Positivo",5),
	AB_NEGATIVO("AB Negativo",6),
	O_POSITIVO("O Positivo",7),
	O_NEGATIVO("O Negativo",8),
	RARO("Raro",0);

	private String nome;
	private int cod;

	TipoSangue(String nome, int cod) {
		this.nome = nome;
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

}