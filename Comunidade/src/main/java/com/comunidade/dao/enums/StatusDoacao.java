package com.comunidade.dao.enums;

public enum StatusDoacao {
	OK("OK",0),
	IMPEDIDO("IMPEDIDO",1),
	BLOQUEADO("BLOQUEADO",2);
	
	private String nome;
	private int cod;

	StatusDoacao(String nome, int cod) {
		this.nome = nome;
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

}
