package com.comunidade.dao.enums;

public enum StatusDoador {

	ATIVO("Ativo",0),
	PASSIVO("Passivo",1),
	DESATIVADO("Desativado",2);

	private String nome;
	private int cod;

	StatusDoador(String nome, int cod) {
		this.nome = nome;
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}
	

}
