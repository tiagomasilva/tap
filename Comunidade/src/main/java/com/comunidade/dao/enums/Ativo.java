package com.comunidade.dao.enums;

public enum Ativo {
	NAO("N�o", 0),
	SIM("Sim", 1);

	private String nome;
	private int cod;

	Ativo(String nome, int cod) {
		this.nome = nome;
		this.cod = cod;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

}
