package com.comunidade.dao.idao;

import com.comunidade.dao.entity.Doador;

public interface IDoadorDao extends IDao<Doador> {

}