package com.comunidade.dao.idao;

import com.comunidade.dao.entity.Doacao;
import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.entity.EstoqueSangue;
import com.comunidade.dao.entity.Notificacao;
import com.comunidade.dao.jinq.GenericDaoJinq;

public class DaoFactory {

	public static IDao<Doacao> getDoacaoDao() {
		return new GenericDaoJinq<>(Doacao.class);
	}
	
	public static IDao<Doador> getDoadorDao() {
		return new GenericDaoJinq<>(Doador.class);
	}
	
	public static IDao<EstoqueSangue> getEstoqueSangueDao() {
		return new GenericDaoJinq<>(EstoqueSangue.class);
	}
	
	public static IDao<Notificacao> getNotificacaoDao() {
		return new GenericDaoJinq<>(Notificacao.class);
	}

}