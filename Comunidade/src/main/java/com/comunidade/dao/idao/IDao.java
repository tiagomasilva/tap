package com.comunidade.dao.idao;

import java.util.List;

import com.comunidade.dao.entity.BaseEntity;
import com.comunidade.dao.jinq.IWhere;


public interface IDao<T extends BaseEntity> {

	void insert(T t);
	void update(T t);
	void delete(int id);
	T find(int id);
	List<T> retrieve();
	List<T> retrieve(IWhere<T> predicate);
}
