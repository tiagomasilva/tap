package com.comunidade.util.deletar;



import javax.mail.MessagingException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailTester {
    public static void main(String[] args) {
    	
    	MailJavaSender msg = new MailJavaSender();
    	String date = getDateTime();
    	String mens = htmlMessagetwo();
    	String Mensagem = msg.pegarHtmlEmail("/mensagempronta.html");
    	mens = mens.replace("{data.mensagem}",date);
    	mens = mens.replace("{doador.nome}", "Filipe");
    	
        MailJava mj = new MailJava();
        //configuracoes de envio
        mj.setSmtpHostMail("smtp.gmail.com");
        mj.setSmtpPortMail("587");
        mj.setSmtpAuth("true");
        mj.setSmtpStarttls("true");
        mj.setUserMail("comunidadesanguebom@gmail.com");
        mj.setFromNameMail("comunidadesanguebom");
        mj.setPassMail("S@ngue!B@m");
        mj.setCharsetMail("ISO-8859-1");
        mj.setSubjectMail("Comunidade Sangue Bom");
        mj.setBodyMail(Mensagem);
        mj.setTypeTextMail(MailJava.TYPE_TEXT_HTML);
        

        //sete quantos destinatarios desejar
        Map<String, String> map = new HashMap<String, String>();
        map.put("novinho@gmail.com", "email gmail");
        map.put("tiagowq3268@gmail.com", "email gmail");
        map.put("comunidadesanguebom@gmail.com", "email gmail");

        mj.setToMailsUsers(map);

        //seta quatos anexos desejar
//        List<String> files = new ArrayList<String>();
//        files.add("C:\images\ajax_loader.gif");
//        files.add("C:\images\hover_next.png");
//        files.add("C:\images\hover_prev.png");

//        mj.setFileMails(files);

        try {
            new MailJavaSender().senderMail(mj);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

   

	private static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
    
    private static String htmlMessage() {
        return
		"<html>\n" +
		"<head>\n" +
		"  <title>Email no formato HTML com Javamail!</title> \n" +
		"</head>\n" +
		"<body>\n" +
		"  <div style='background-color:orange; width:28%; height:100px;'>\n" +
		"   <ul>\n" +
		"    <li>Leia o novo tutorial JavaMail do Programando com Java.</li>\n" +
		"    <li>Aprenda como enviar emails com anexos.</li>\n" +
		"    <li>Aprenda a enviar emails em formato texto simples ou html.</li> \n" +
		"    <li>Aprenda como enviar seu email para mais de um destin&aacute;tario.</li>\n" +
		"   </ul>\n" +
		"   <p>Visite o blog \n" +
		"     <a href='http://mballem.wordpress.com/'>Programando com Java</a>\n" +
		"   </p>\n" +
		"  </div>\n" +
		"  <div style='width:28%; height:50px;' align='center'>\n" +
		"    Download do JavaMail<br/>\n" +
		"    <a href='http://www.oracle.com/technetwork/java/javaee/index-138643.html'>\n" +
		"      <img src='http://www.oracleimg.com/admin/images/ocom/hp/oralogo_small.gif'/>\n" +
		"    </a> \n" +
		"  </div>\n" +
		"</body> \n" +
		"</html>";
    }
    
   

    
    private static String htmlMessagetwo() {
        return
       
        "<html>\n"+
        "<body>\n"+

        "<div style='background: #FFFFFF; font-family: Verdana, Arial, Tahoma; font-size: 10pt; border: 1px solid #d9d9d9; width: 450px; margin: 10px auto;'>\n"+
        "<div style='width: auto;'>\n"+
		"<img img src='http://i67.tinypic.com/i26jyw.png' border='1'  />\n"+
		"</div>\n"+
        		"<div style='background: #DDD; text-align: center; padding: 3px;'>\n"+
        			"<strong>Informativo</strong>\n"+
        		"</div>\n"+
        		"<div style='padding: 10pt;'>\n"+
        			"<p>\n"+
        				"Prezado doador(a) <strong> {doador.nome} </strong>\n"+
        			"</p>\n"+

        			"<p>Informamos que nosso banco de sangue esta baixo para seu tipo de seu sangue.</p>\n"+

        			"<p>Se estiver disponibilidade para doar agradecemos!</p>\n"+

        			"<p>Doe Sangue, Salve Vidas!</p>\n"+

        			"<p>Recife, {data.mensagem} </p>\n"+
        		"</div>\n"+
        	"</div>\n"+

        "<body>\n"+
        "</html>";
        		
    }
}