package com.comunidade.util.deletar;

import javax.mail.MessagingException;

import com.comunidade.dao.entity.Doador;
import com.comunidade.properties.PropertiesUtil;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailHtml {
	public static void conectaEmail(Map<String, String> mapIn, boolean personalizada) {

		MailJavaSender msg = new MailJavaSender();
		String date = getDateTime();
		String mensagenHtml = htmlMessagetwo();
//		String mensagenHtmlRead = msg.pegarHtmlEmail("C:/Users/Tiago Malaquias/git/tap/Comunidade/src/main/java/com/comunidade/util/email/mensagempronta.html");
		mensagenHtml = mensagenHtml.replace("{data.mensagem}", date);

		MailJava emailOut = new MailJava();
		// configuracoes de envio
		emailOut.setSmtpHostMail("smtp.gmail.com");
		emailOut.setSmtpPortMail("587");
		emailOut.setSmtpAuth("true");
		emailOut.setSmtpStarttls("true");
		emailOut.setUserMail("comunidadesanguebom@gmail.com");
		emailOut.setFromNameMail("comunidadesanguebom");
		emailOut.setPassMail("S@ngue!B@m");
		emailOut.setCharsetMail("ISO-8859-1");
		emailOut.setSubjectMail("Comunidade Sangue Bom");
		emailOut.setBodyMail(mensagenHtml);
		emailOut.setTypeTextMail(MailJava.TYPE_TEXT_HTML);

		// sete quantos destinatarios desejar
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("novinho@gmail.com", "email gmail");
//		map.put("tiagowq3268@gmail.com", "email gmail");
//		map.put("comunidadesanguebom@gmail.com", "email gmail");

		emailOut.setToMailsUsers(mapIn);
		if(!personalizada){
		}
		
//		mensagenHtml = mensagenHtml.replace("{doador.nome}", "Filipe");
		
		// seta quatos anexos desejar
		// List<String> files = new ArrayList<String>();
		// files.add("C:\images\ajax_loader.gif");
		// files.add("C:\images\hover_next.png");
		// files.add("C:\images\hover_prev.png");

		// mj.setFileMails(files);

		try {
			new MailJavaSender().senderMail(emailOut);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public static void conectaEmail(Doador doadorIn, boolean personalizada) {

		MailJavaSender msg = new MailJavaSender();
		String date = getDateTime();
		String mensagenHtml = htmlMessagetwo();
//		String mensagenHtmlRead = msg.pegarHtmlEmail("C:/Users/Tiago Malaquias/git/tap/Comunidade/src/main/java/com/comunidade/util/email/mensagempronta.html");
		mensagenHtml = msg.pegarHtmlEmail("TemplateRecuperarSenha.html");//PropertiesUtil.getArquivo("template.email.recuperarSenha"));
		mensagenHtml = mensagenHtml.replace("{doador.nome}", doadorIn.getNome());
		mensagenHtml = mensagenHtml.replace("{doador.senha}", doadorIn.getSenha());

		MailJava emailOut = new MailJava();
		// configuracoes de envio
		emailOut.setSmtpHostMail("smtp.gmail.com");
		emailOut.setSmtpPortMail("587");
		emailOut.setSmtpAuth("true");
		emailOut.setSmtpStarttls("true");
		emailOut.setUserMail("comunidadesanguebom@gmail.com");
		emailOut.setFromNameMail("comunidadesanguebom");
		emailOut.setPassMail("S@ngue!B@m");
		emailOut.setCharsetMail("ISO-8859-1");
		emailOut.setSubjectMail("Comunidade Sangue Bom");
		emailOut.setBodyMail(mensagenHtml);
		emailOut.setTypeTextMail(MailJava.TYPE_TEXT_HTML);

		// sete quantos destinatarios desejar
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("novinho@gmail.com", "email gmail");
//		map.put("tiagowq3268@gmail.com", "email gmail");
//		map.put("comunidadesanguebom@gmail.com", "email gmail");
		
		Map<String, String> mapIn = new HashMap<String, String>();
		mapIn.put(doadorIn.getEmail(), doadorIn.getNome());
		
		emailOut.setToMailsUsers(mapIn);
		if(!personalizada){
		}
		
//		mensagenHtml = mensagenHtml.replace("{doador.nome}", "Filipe");
		
		// seta quatos anexos desejar
		// List<String> files = new ArrayList<String>();
		// files.add("C:\images\ajax_loader.gif");
		// files.add("C:\images\hover_next.png");
		// files.add("C:\images\hover_prev.png");

		// mj.setFileMails(files);

		try {
			new MailJavaSender().senderMail(emailOut);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	private static String htmlMessage() {
		return "<html>\n" + "<head>\n" + "  <title>Email no formato HTML com Javamail!</title> \n" + "</head>\n"
				+ "<body>\n" + "  <div style='background-color:orange; width:28%; height:100px;'>\n" + "   <ul>\n"
				+ "    <li>Leia o novo tutorial JavaMail do Programando com Java.</li>\n"
				+ "    <li>Aprenda como enviar emails com anexos.</li>\n"
				+ "    <li>Aprenda a enviar emails em formato texto simples ou html.</li> \n"
				+ "    <li>Aprenda como enviar seu email para mais de um destin&aacute;tario.</li>\n" + "   </ul>\n"
				+ "   <p>Visite o blog \n" + "     <a href='http://mballem.wordpress.com/'>Programando com Java</a>\n"
				+ "   </p>\n" + "  </div>\n" + "  <div style='width:28%; height:50px;' align='center'>\n"
				+ "    Download do JavaMail<br/>\n"
				+ "    <a href='http://www.oracle.com/technetwork/java/javaee/index-138643.html'>\n"
				+ "      <img src='http://www.oracleimg.com/admin/images/ocom/hp/oralogo_small.gif'/>\n" + "    </a> \n"
				+ "  </div>\n" + "</body> \n" + "</html>";
	}

	static String htmlMessagetwo() {
		return

		"<html>\n" + "<body>\n" +

				"<div style='background: #FFFFFF; font-family: Verdana, Arial, Tahoma; font-size: 10pt; border: 1px solid #d9d9d9; width: 450px; margin: 10px auto;'>\n"
				+ "<div style='width: auto;'>\n" + "<img img src='http://i67.tinypic.com/i26jyw.png' border='1'  />\n"
				+ "</div>\n" + "<div style='background: #DDD; text-align: center; padding: 3px;'>\n"
				+ "<strong>Informativo</strong>\n" + "</div>\n" + "<div style='padding: 10pt;'>\n" + "<p>\n"
				+ "Prezado doador(a) <strong> {doador.nome} </strong>\n" + "</p>\n" +

				"<p>Informamos que nosso banco de sangue esta baixo para seu tipo de seu sangue.</p>\n" +

				"<p>Se estiver disponibilidade para doar agradecemos!</p>\n" +

				"<p>Doe Sangue, Salve Vidas!</p>\n" +

				"<p>Recife, {data.mensagem} </p>\n" + "</div>\n" + "</div>\n" +

				"<body>\n" + "</html>";

	}
}