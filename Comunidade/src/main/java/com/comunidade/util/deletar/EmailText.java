package com.comunidade.util.deletar;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class EmailText {

	private static final String HOSTNAME = "smtp.gmail.com";
	private static final String USERNAME = "comunidadesanguebom";
	private static final String PASSWORD = "S@ngue!B@m";
	private static final String EMAILORIGEM = "comunidadesanguebom@gmail.com";

	public static Email conectaEmail() throws EmailException {
		Email email = new SimpleEmail();
		email.setHostName(HOSTNAME);
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator(USERNAME, PASSWORD));
		email.setTLS(true);
		email.setFrom(EMAILORIGEM);
		return email;
	}

	public static String enviaEmailTexto(Mensagem mensagem) throws EmailException {
		if (mensagem == null || mensagem.getDestinos().isEmpty()) {
			return null;
		}
		Email email = conectaEmail();
		email.setSubject(mensagem.getTitulo());
		email.setMsg(mensagem.getMensagem());
		for (String destino : mensagem.getDestinos()) {
			email.addTo(destino);
		}
		return email.send();
	}
}