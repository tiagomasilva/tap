package com.comunidade.util.deletar;

import java.util.List;

public class Mensagem {

	private String titulo;
	private String mensagem;
	private List<String> destinos;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<String> getDestinos() {
		return destinos;
	}

	public void setDestinos(List<String> destinos) {
		this.destinos = destinos;
	}
	
	public void addDestino(String destino) {
		this.destinos.add(destino);
	}
	
}