package com.comunidade.util.email;

import java.util.Map;

import com.comunidade.dao.entity.Doador;

public class EmailComunidadeMult extends EmailComunidade implements IEmailComunidade{

	public EmailComunidadeMult(String url, Map<String, Doador> doadores, boolean globalMenssagem) {
		setTemplateMensagem(readTemplate(url));
		setGlobalMenssagem(globalMenssagem);
		setDoadores(doadores);
		setDataTimeUnico(gerarDateTime());
	}
	
}