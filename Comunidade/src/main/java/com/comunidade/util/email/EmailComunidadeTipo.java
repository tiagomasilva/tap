package com.comunidade.util.email;

public enum EmailComunidadeTipo {
	RECUPERAR_SENHA("Recuperar Senha",0),
	NOTIFICACAO_BB_SEM_GRAFICO("Notificação Banco Baixo sem Grafico",1),
	NOTIFICACAO_BB_COM_GRAFICO("Notificação Banco Baixo com Grafico",2);
	
	private String nome;
	private int cod;

	EmailComunidadeTipo(String nome, int cod) {
		this.nome = nome;
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

}
