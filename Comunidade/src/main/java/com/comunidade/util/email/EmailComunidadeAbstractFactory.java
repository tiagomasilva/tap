package com.comunidade.util.email;

import java.util.Map;

import com.comunidade.dao.entity.Doador;
import com.comunidade.properties.PropertiesUtil;

public class EmailComunidadeAbstractFactory {
	public static IEmailComunidade getInstancia(Map<String, Doador> doadores,
			EmailComunidadeTipo emailComunidadeTipo, boolean globalMenssagem) {

		switch (emailComunidadeTipo) {
		
		case RECUPERAR_SENHA:
			return new EmailComunidadeMult(PropertiesUtil.getArquivo("template.email.recuperarSenha"), doadores, globalMenssagem);
			
		case NOTIFICACAO_BB_SEM_GRAFICO:
			return new EmailComunidadeMult(PropertiesUtil.getArquivo("template.email.info.banco"), doadores, globalMenssagem);

		case NOTIFICACAO_BB_COM_GRAFICO:
			break;
		
		default:
			break;

		}

		return null;
	}

}
