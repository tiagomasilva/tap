package com.comunidade.util.email;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.comunidade.dao.entity.Doador;
import com.comunidade.util.Util;

public abstract class EmailComunidade implements IEmailComunidade {

	// CONSTANTES
	private static final String PROTOCOLO = "smtp";
	private static final String SMTP_PORT_MAIL = "587";
	private static final String SMTP_HOST_MAIL = "smtp.gmail.com";
	private static final String SMTP_AUTH = "true";
	private static final String SMTP_STARTTLS = "true";
	private static final String TYPE_TEXT_MAIL = "text/html";
	private static final String CHARSET_MAIL = "ISO-8859-1";
	private static final String USER_MAIL = "comunidadesanguebom@gmail.com";
	private static final String PASS_MAIL = "S@ngue!B@m";

	private static final String FROM_NAME_MAIL = "comunidadesanguebom";
	private static final String SUBJECT_MAIL = "Comunidade Sangue Bom";

	// GLOBAL VARIAVEIS
	private static Session sessionConection;

	// Mensagem
	private String templateMensagem;
	private boolean globalMenssagem;
	private Map<String, Doador> doadores = new HashMap<String, Doador>();
	private Map<String, Map<String, String>> emailMenssagem = new HashMap<String, Map<String, String>>();
	private String dataTimeUnico;

	// Getters e Setters

	public String getTemplateMensagem() {
		return templateMensagem;
	}

	public void setTemplateMensagem(String templateMensagem) {
		this.templateMensagem = templateMensagem;
	}

	public Map<String, Doador> getDoadores() {
		return doadores;
	}

	public void setDoadores(Map<String, Doador> doadores) {
		this.doadores = doadores;
	}

	public Map<String, Map<String, String>> getEmailMenssagem() {
		return emailMenssagem;
	}

	public void setEmailMenssagem(Map<String, Map<String, String>> emailMenssagem) {
		this.emailMenssagem = emailMenssagem;
	}

	public boolean isGlobalMenssagem() {
		return globalMenssagem;
	}

	public void setGlobalMenssagem(boolean globalMenssagem) {
		this.globalMenssagem = globalMenssagem;
	}

	public String getDataTimeUnico() {
		return dataTimeUnico;
	}

	public void setDataTimeUnico(String dataTimeUnico) {
		this.dataTimeUnico = dataTimeUnico;
	}

	static String gerarDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static Session getSessionConectionEmail() {
		if (sessionConection == null) {
			sessionConection = conectaComServidorDeEmail();
		}
		return sessionConection;
	}

	public static void getSessionConectionEmail(Session sessionConection) {
		EmailComunidade.sessionConection = sessionConection;
	}

	// RECEBE:Local onde Esta o arquivo;
	public String readTemplate(String url) {
		if (Util.isNull(url)) {
			return "";
		}
		String[] tempUrl = url.split("/");
		url = tempUrl[tempUrl.length - 1];

		InputStream is = getClass().getResourceAsStream(url);
		BufferedInputStream bis = new BufferedInputStream(is);
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		int result;
		try {
			result = bis.read();
			while (result != -1) {
				byte b = (byte) result;
				buf.write(b);
				result = bis.read();
			}
			return buf.toString("ISO-8859-1");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String replaceMenssagemSingular(Doador doador, String menssagemIn) {

		if (doador == null) {
			return "";
		}

		if (doador.getNome() != null) {
			menssagemIn = menssagemIn.replace("{{doador.nome}}", doador.getNome());
		} else {
			menssagemIn = menssagemIn.replace("{{doador.nome}}", "");
		}

		if (doador.getSenha() != null) {
			menssagemIn = menssagemIn.replace("{{doador.senha}}", doador.getSenha());
		} else {
			menssagemIn = menssagemIn.replace("{{doador.senha}}", "");
		}

		// Sistema
		menssagemIn = menssagemIn.replace("{{mensagem.data}}", getDataTimeUnico());

		return menssagemIn;
	}

	public static Session conectaComServidorDeEmail() {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", PROTOCOLO);
		props.setProperty("mail.host", SMTP_HOST_MAIL);
		props.setProperty("mail.smtp.auth", SMTP_AUTH);
		props.setProperty("mail.smtp.starttls.enable", SMTP_STARTTLS);
		props.setProperty("mail.smtp.port", SMTP_PORT_MAIL);
		props.setProperty("mail.mime.charset", CHARSET_MAIL);

		// classe anonima que realiza a autenticacao
		// do usuario no servidor de email.
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USER_MAIL, PASS_MAIL);
			}
		};

		// Cria a sessao passando as propriedades e a autenticacao
		Session session = Session.getDefaultInstance(props, auth);
		// Gera um log no console referente ao processo de envio
		session.setDebug(true);

		return session;
	}

	// public void conectaEmail(Doador doadorIn, boolean personalizada) {
	//
	// MailJava emailOut = new MailJava();
	//
	// // configuracoes de envio
	// emailOut.setSmtpHostMail("smtp.gmail.com");
	// emailOut.setSmtpPortMail("587");
	// emailOut.setSmtpAuth("true");
	// emailOut.setSmtpStarttls("true");
	// emailOut.setUserMail("comunidadesanguebom@gmail.com");
	// emailOut.setFromNameMail("comunidadesanguebom");
	// emailOut.setPassMail("S@ngue!B@m");
	// emailOut.setCharsetMail("ISO-8859-1");
	// emailOut.setSubjectMail("Comunidade Sangue Bom");
	// emailOut.setTypeTextMail(MailJava.TYPE_TEXT_HTML);
	//
	// String mensagenHtml = getTemplateMensagem();
	// emailOut.setBodyMail(mensagenHtml);
	//
	// Map<String, String> mapIn = new HashMap<String, String>();
	// mapIn.put(doadorIn.getEmail(), doadorIn.getNome());
	//
	// emailOut.setToMailsUsers(mapIn);
	// if (!personalizada) {
	// }
	//
	// try {
	// senderMail(emailOut);
	// } catch (UnsupportedEncodingException e) {
	// e.printStackTrace();
	// } catch (MessagingException e) {
	// e.printStackTrace();
	// }
	// }

//	public void senderMail(final MailJava mail) throws UnsupportedEncodingException, MessagingException {
//
//		// cria a mensagem setando o remetente e seus destinatarios
//		Message mensagemOut = new MimeMessage(conectaComServidorDeEmail());
//
//		String global = mail.getBodyMail();
//		String date = gerarDateTime();
//
//		/** Podem ser Modificads */
//
//		// aqui seta o remetente
//		mensagemOut.setFrom(new InternetAddress(USER_MAIL, FROM_NAME_MAIL));
//
//		// Adiciona um Assunto a Mensagem
//		mensagemOut.setSubject(SUBJECT_MAIL);
//		/**
//		 * */
//
//		for (Map.Entry<String, String> mapTemp : mail.getToMailsUsers().entrySet()) {
//			mail.setToMailsUsers(new HashMap<String, String>());
//			mail.getToMailsUsers().put(mapTemp.getKey(), mapTemp.getValue());
//
//			mail.setBodyMail(global.replace("{doador.nome}", mapTemp.getValue()));
//
//			boolean first = true;
//			for (Map.Entry<String, String> map : mail.getToMailsUsers().entrySet()) {
//				if (first) {
//					// setamos o 1&deg; destinatario
//					mensagemOut.addRecipient(Message.RecipientType.TO,
//							new InternetAddress(map.getKey(), map.getValue()));
//					first = false;
//				} else {
//					// setamos os demais destinatarios
//					mensagemOut.addRecipient(Message.RecipientType.CC,
//							new InternetAddress(map.getKey(), map.getValue()));
//				}
//			}
//
//			// Cria o objeto que recebe o texto do corpo do email
//			MimeBodyPart textPart = new MimeBodyPart();
//			textPart.setContent(mail.getBodyMail(), TYPE_TEXT_MAIL);
//
//			// Monta a mensagem SMTP inserindo o conteudo, texto e anexos
//			Multipart mps = new MimeMultipart();
//
//			// adiciona o corpo texto da mensagem
//			mps.addBodyPart(textPart);
//
//			// adiciona a mensagem o conteudo texto e anexo
//			mensagemOut.setContent(mps);
//
//			// Envia a Mensagem
//			Transport.send(mensagemOut);
//		}
//	}

	public void send() {
		try {
			// cria a mensagem setando o remetente e seus destinatarios
			Message mensagemOut = new MimeMessage(conectaComServidorDeEmail());
			/** Podem ser Modificads */
			// aqui seta o remetente
//			mensagemOut.setFrom(new InternetAddress(USER_MAIL, FROM_NAME_MAIL));
			// Adiciona um Assunto a Mensagem
			mensagemOut.setSubject(SUBJECT_MAIL);

			for (Map.Entry<String, Doador> doadorTemp : getDoadores().entrySet()) {

				mensagemOut.setFrom(new InternetAddress(doadorTemp.getKey(), doadorTemp.getValue().getNome()));
				mensagemOut.addRecipient(Message.RecipientType.TO, new InternetAddress(doadorTemp.getKey(), doadorTemp.getValue().getNome()));

				// Cria o objeto que recebe o texto do corpo do email
				MimeBodyPart textPart = new MimeBodyPart();
				textPart.setContent(replaceMenssagemSingular(doadorTemp.getValue(), getTemplateMensagem()),
						TYPE_TEXT_MAIL);

				// Monta a mensagem SMTP inserindo o conteudo, texto e anexos
				Multipart mps = new MimeMultipart();

				// adiciona o corpo texto da mensagem
				mps.addBodyPart(textPart);

				// adiciona a mensagem o conteudo texto e anexo
				mensagemOut.setContent(mps);

				// Envia a Mensagem
				Transport.send(mensagemOut);

			}

		} catch (Error | UnsupportedEncodingException | MessagingException er) {
			er.printStackTrace();

		}
	}
}