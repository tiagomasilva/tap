package test.comunidade.daoentity;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.Deletado;
import com.comunidade.dao.enums.StatusDoacao;
import com.comunidade.dao.enums.StatusDoador;
import com.comunidade.dao.enums.TipoSangue;
import com.comunidade.dao.enums.TipoUsuario;
import com.comunidade.dao.idao.DaoFactory;
import com.comunidade.mb.DoacaoMB;

public class Teste {

	public static void main(String[] args) {
//		DoacaoMB dm = new DoacaoMB();
//		dm.addDias(StatusDoacao.OK);
		
//		StatusDoacao in = StatusDoacao.valueOf(StatusDoacao.IMPEDIDO.getNome());
//		Calendar calendar = Calendar.getInstance();
//		int dias = 0;
//
//		switch (in) {
//		case OK:
//			dias = 90;
//			break;
//		case IMPEDIDO:
//			dias = 15;
//			break;
//		case BLOQUEADO:
//			dias = 100000;
//			break;
//
//		default:
//			dias = 0;
//			break;
//		}
//
//		calendar.add(Calendar.DATE, dias);
//		Date date = Date.from(calendar.toInstant());
		
		
		// Doador doador = new Doador();
		// doador.setId(14);
		// doador.setId(15);
		// FacadeDoador.deletarDoador(doador);
		
		createDoadores();
		
		// System.out.println(FacadeDoador.buscarPorCPF("570.768.302-05").getNome());
		// System.out.println(Util.validarCPF("096.963.724-18"));
		// System.out.println(Util.validarCPF("06.963.724-18"));
		// System.out.println(Util.validarCPF("096.963.724-180"));
		// System.out.println(Util.validarCPF("0AB.963.724-18"));
	}

	public static void createDoadores() {
		createDoador("tiago@gmail", "123123", "tiagowq@hotmail.com", TipoUsuario.DOADOR, "Tiago Malaquias",
				Date.from(Instant.now()), "163.633.017-77", StatusDoador.ATIVO, TipoSangue.A_POSITIVO.getNome(),
				Deletado.NAO.getNome(), Date.from(Instant.now()));

		createDoador("filipe@gmail", "123123", "novinho@gmail.com", TipoUsuario.DOADOR, "Filipe Gomes",
				Date.from(Instant.now()), "776.949.845-70", StatusDoador.ATIVO, TipoSangue.B_NEGATIVO.getNome(),
				Deletado.NAO.getNome(), Date.from(Instant.now()));

		createDoador("hemope@gmail", "123123", "comunidadesanguebom@gmail.com", TipoUsuario.ADM, "Carlos Sergio",
				Date.from(Instant.now()), "570.768.302-05", StatusDoador.ATIVO, TipoSangue.RARO.getNome(),
				Deletado.NAO.getNome(), Date.from(Instant.now()));

	}

	private static void createDoador(String login, String senha, String email, TipoUsuario tipoUsuario, String nome,Date dataNasci,String cpf, StatusDoador statusDoador, String tipoSangue,String deletado, Date proxDoacao) {
		Doador doador = new Doador();
		doador.setLogin(login);
		doador.setSenha(senha);
		doador.setEmail(email);
		doador.setTipoUsuario(tipoUsuario);
		doador.setNome(nome);
		doador.setDataNasci(dataNasci);
		doador.setCpf(cpf);
		doador.setTipoSangue(tipoSangue);
		doador.setStatusDoador(statusDoador);
		doador.setDoacoes(null);
		doador.setProxDoacao(proxDoacao);
		doador.setDeletado(deletado);

		DaoFactory.getDoadorDao().insert(doador);
	}

	public static void createDoador(Doador doadorIn) {
		Doador doador = new Doador();
		doador.setNome(doadorIn.getNome());
		doador.setCpf(doadorIn.getCpf());
		doador.setStatusDoador(doadorIn.getStatusDoador());
		doador.setTipoSangue(doadorIn.getTipoSangue());

		DaoFactory.getDoadorDao().insert(doador);
	}

	public static void createDoadorClone(Doador doadorIn) {
		// Doador doador = new Doador();
		// doador.setNome(doadorIn.getNome());
		// doador.setCpf(doadorIn.getCpf());
		// doador.setStatusDoador(doadorIn.getStatusDoador());
		// doador.setTipoSangue(doadorIn.getTipoSangue());

		DaoFactory.getDoadorDao().insert(doadorIn);
	}

}
