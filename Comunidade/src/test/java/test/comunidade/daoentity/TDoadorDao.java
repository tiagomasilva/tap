package test.comunidade.daoentity;

import org.junit.Test;

import com.comunidade.dao.entity.Doador;
import com.comunidade.dao.enums.StatusDoador;
import com.comunidade.dao.enums.TipoSangue;
import com.comunidade.dao.enums.TipoUsuario;
import com.comunidade.dao.idao.DaoFactory;

public class TDoadorDao {

	@Test
	public void testInsert() {
		createDoador("tiago", "123123", TipoUsuario.DOADOR, "Tiago Malaquias", "88778956", StatusDoador.ATIVO, TipoSangue.A_POSITIVO.getNome());
		createDoador("filipe", "123123", TipoUsuario.DOADOR, "Filipe Gomes", "09778956478", StatusDoador.ATIVO, TipoSangue.B_POSITIVO.getNome());
		createDoador("maria", "123123", TipoUsuario.DOADOR, "Maria Rosa", "65268295624", StatusDoador.ATIVO, TipoSangue.B_POSITIVO.getNome());
		createDoador("pedro", "123123", TipoUsuario.DOADOR, "Pedro Sabino", "33938141760", StatusDoador.ATIVO, TipoSangue.B_POSITIVO.getNome());
		
		createDoador("admin", "123123", TipoUsuario.ADM, "Arthur Marttines", "08778956478", StatusDoador.ATIVO, TipoSangue.B_POSITIVO.getNome());
		createDoador("hemope@gmail", "123123", TipoUsuario.ADM, "Carlos Sergio", "72617455700", StatusDoador.ATIVO, TipoSangue.B_POSITIVO.getNome());
	}

	@Test
	public void testUpdate() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFind() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRetrieve() {
		// fail("Not yet implemented");
	}

	// Fun��es
	private static void createDoador(String login, String senha, TipoUsuario tipoUsuario, String nome, String cpf,
			StatusDoador statusDoador, String tipoSangue) {
		
		Doador doador = new Doador();
		doador.setLogin(login);
		doador.setSenha(senha);
		doador.setTipoUsuario(tipoUsuario);
		doador.setNome(nome);
		doador.setCpf(cpf);
		doador.setTipoSangue(tipoSangue);
		doador.setStatusDoador(statusDoador);

		DaoFactory.getDoadorDao().insert(doador);
	}

}
